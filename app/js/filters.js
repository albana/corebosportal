'use strict';

/* Filters */

angular.module('coreBOSJSTickets.filters', [])
  .filter('getArrayElementById', function() {
    return function(input, idvalue, idprop) {
      if (idprop === undefined) idprop = 'id';
      var i = 0,
        len = input.length;
      for (; i < len; i++) {
        if (input[i][idprop] == idvalue) {
          return input[i];
        }
      }
      return null;
    };
  })
  .filter('getArrayType10', function() {
    return function(input) {
      var i = 0,
        len = input.length;
      var ui10_fields = new Array();
      for (; i < len; i++) {
        if (input[i]['type']['name'] == 'reference') {
          ui10_fields.push(input[i]);
        }
      }
      return ui10_fields;
    };
  })
  .filter('getPickListDep', function() {
    return function(input,map_field_dep,columns,MAP_PCKLIST_TARGET,moduleData) {
      var i = 0,
        len = input.length;
      var records = new Array();
      for (; i < len; i++) {
          if(MAP_PCKLIST_TARGET.indexOf(columns)!==-1){
              angular.forEach(map_field_dep, function(value, key) {
                  if(value.target_picklist.indexOf(columns)!==-1){
                      var conditionResp = '';
                      angular.forEach(value.respfield, function(resp_val, resp_val_key) {
                            var resp_value = value.respvalue_portal[resp_val_key];
                            var comparison = value.comparison[resp_val_key];
                            if (resp_val_key !== 0 ){
                                if (comparison === 'empty' || comparison === 'notempty')
                                    conditionResp += ' || ';
                                else
                                    conditionResp += ' && ';
                            }
                            if (comparison === 'empty')
                              conditionResp += (moduleData[resp_val] === '' || moduleData[resp_val] === undefined);
                            else if (comparison === 'notempty')
                              conditionResp += (moduleData[resp_val] !== '' && moduleData[resp_val] !== undefined);
                            else{
                              conditionResp += (resp_value.indexOf(moduleData[resp_val])!= -1 && moduleData[resp_val]!=undefined);
                            }
                      });
                      if ( eval(conditionResp) ) 
                      {
                          angular.forEach(value.target_picklist_values[columns], function(targ_val, targ_key) {
                             if(input[i]['value']==targ_val){
                                  records.push(input[i]);
                              }       
                          });
                      }
                  }
            });
          }
          else{
              records.push(input[i]);
          }
      }
      return records;
    };
  })
.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}])
  .filter('getTableRecords', function() {
    return function(input, column, val) {
      var i = 0,
        len = input.length;
      var records = new Array();
      for (; i < len; i++) {
        if (input[i][column[0]].indexOf(val) != -1 || input[i][column[1]].indexOf(val) != -1 || input[i][column[2]].indexOf(val) != -1 || input[i][column[3]].indexOf(val) != -1 || input[i][column[4]].indexOf(val) != -1) {
          records.push(input[i]);
        }
      }
      return records;
    };
  })
  .filter('nospace', function() {
    /**
     * Description: removes white space from text. useful for html values that cannot have spaces
     * Usage: {{some_text | nospace}}
     */
    return function(value) {
      return (!value) ? '' : value.replace(/ /g, '');
    };
  })
  .filter("formatNameInfo", function() {
    return function(label, name) {
      return '<b>' + label + '</b><br>' + name;
    };
  })
  .filter("formatFieldInfo", function() {
    return function(field) {
      var finfo = '';
      if (!angular.isUndefined(field)) {
        finfo = finfo + 'Mandatory: ' + (field.mandatory ? 'yes' : 'no') + '<br>';
        finfo = finfo + 'Null: ' + (field.nullable ? 'yes' : 'no') + '<br>';
        finfo = finfo + 'Editable: ' + (field.editable ? 'yes' : 'no');
        finfo = finfo + (angular.isUndefined(field.sequence) ? '' : '<br>Sequence: ' + field.sequence);
      }
      return finfo;
    };
  })
  .filter("formatBlockInfo", function() {
    return function(block) {
      var binfo = '';
      if (!angular.isUndefined(block)) {
        binfo = 'ID: ' + block.blockid + '<br>';
        binfo = binfo + 'Sequence: ' + block.blocksequence + '<br>';
        binfo = binfo + 'Label: ' + block.blocklabel + '<br>';
        binfo = binfo + 'Name: ' + block.blockname;
      }
      return binfo;
    };
  })
  .filter("formatTypeInfo", function() {
    return function(field) {
      var tinfo = '';
      if (!angular.isUndefined(field.type)) {
        tinfo = 'Type: ' + field.type.name;
        tinfo = tinfo + (angular.isUndefined(field.typeofdata) ? '' : '&nbsp;(' + field.typeofdata + ')');
        tinfo = tinfo + (angular.isUndefined(field.uitype) ? '' : '<br>UIType: ' + field.uitype);
        tinfo = tinfo + (angular.isUndefined(field.type.format) ? '' : '<br>Format: ' + field.type.format);
        tinfo = tinfo + (angular.isUndefined(field['default']) ? '' : '<br>Default: ' + field['default']);
      }
      return tinfo;
    };
  })
.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
})
  .filter("formatReferenceInfo", function() {
    return function(reference) {
      var rinfo = '';
      if (!angular.isUndefined(reference.refersTo)) {
        angular.forEach(reference.refersTo, function(value, key) {
          rinfo = rinfo + value + ', ';
        });
      }
      if (!angular.isUndefined(reference.picklistValues)) {
        angular.forEach(reference.picklistValues, function(value, key) {
          rinfo = rinfo + value.label + ': ' + value.value + ', ';
        });
      }
      return rinfo.substring(0, rinfo.length - 2);
    };
  })
  .filter("formatModuleFields", function($filter) {
    return function(mfields) {
      var rinfo = [];
      if (!angular.isUndefined(mfields)) {
        angular.forEach(mfields, function(value, key) {
          var finfo = {
            labelinfo: $filter('formatNameInfo')(value.label, value.name),
            fieldinfo: $filter('formatFieldInfo')(value),
            blockinfo: $filter('formatBlockInfo')(value.block),
            typeinfo: $filter('formatTypeInfo')(value),
            refinfo: $filter('formatReferenceInfo')(value.type)
          };
          rinfo.push(finfo);
        });
      }
      return rinfo;
    };
  })
  .filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    };
  }]);
