'use strict';

/* Controllers */

angular.module('coreBOSJSTickets.controllers', [])

.controller('navigationCtrl', function($scope, $rootScope, Setup, $i18next, $window, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.appname = Setup.app;
    $scope.messageurl = "http://corebos.org";
    $scope.messagetxt = "Data provided by coreBOS. &copy; 2014";
    $(window).bind('resize', function() {
        if ($window.innerWidth < 768 || $location.path() == '/login') {
            $scope.menuShow = false;
            $rootScope.menuShow = false;
        } else {
            $scope.menuShow = true;
            $rootScope.menuShow = true;
        }
        $scope.$apply();
    });
    $scope.iscustomer = true;
    coreBOSWSAPI.getMenuStructure(coreBOSAPIStatus.getSessionInfo()._userid).then(function(response) {
        $scope.selectedmodule = [];
        if (response.data.result != undefined && response.data.result != '') {
            angular.forEach(response.data.result, function(value, key) {
                var menu = {
                    title: key,
                    values: value
                };
                $scope.selectedmodule.push(menu);
            });
        } else {
            $scope.iscustomer = false;
            coreBOSWSAPI.doListTypes().then(function(response) {
                $scope.listtypes = response.data.result;
                var ltypes = coreBOSWSAPI.processListTypes(response.data.result.information, true, true);
                $scope.selecttypes = ltypes;
            });
        }
    });
    if ($location.path() == '/login')
        $scope.menuShow = false;
    else
        $scope.menuShow = true;
    $scope.toggleMenu = function() {
        $scope.menuShow = !$scope.menuShow;
        $rootScope.menuShow = !$rootScope.menuShow;
    };
    $rootScope.$watch("menuShow", function(newval, oldval) {
        $scope.menuShow = $rootScope.menuShow;
    });
    $scope.isActive = function(viewLocation, viewLocation2) {
        var active = (viewLocation === $location.path() || viewLocation2 === $location.path());
        return active;
    };
})

.controller('DocsRelated', function($scope, $i18next, $http, $routeParams, FileUploader, $filter, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.ulogged = coreBOSAPIStatus.getSessionInfo()._userid;
    $scope.uid = $scope.ulogged.split("x");
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://193.182.16.151/SAME/upload.php?user=' + $scope.uid[1]
    });

    // FILTERS
    $scope.width = screen.width;
    $scope.height = screen.height;
    $scope.seconddiv = screen.width / 2 - 200;
    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            return this.queue.length < 10;
        }
    });
    $scope.removeitem = function(item) {
        // console.log("removejustcreate" + $scope.just_created_doc);
        // console.log("filesize" + JSON.stringify(item.size));
        // console.log("filename" + JSON.stringify(item.name));
        // console.log("filetype" + JSON.stringify(item.type));
        var size = JSON.stringify(item.size);
        var name = JSON.stringify(item.name);
        var type = JSON.stringify(item.type);
        $http.get("http://193.182.16.151/SAME/deleteit.php?idtoremove=" + $scope.just_created_doc.split('@@')[0] + '&size=' + size + '&name=' + name + '&type=' + type).success(function(data) {});
    }

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.info('onSuccessItem', fileItem, response, status, headers);
        // console.log("controllerdocs");
        $scope.just_created_doc = response;
        var resp_doc = $scope.just_created_doc.split('@@');
        // console.log('tjetercontrollerdocs' + resp_doc + JSON.stringify($scope.moduleData.messagetocase));
        $http.get('http://193.182.16.151/SAME/uploadrelated.php?param1=' + $scope.moduleData.messagetocase.split("x")[1] + '&param2=' + resp_doc[0]).success(function(data) {
            $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
        });
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.just_created_doc = response;
        var resp_doc = $scope.just_created_doc.split('@@');
        // console.log('tjetercontrollerdocs2' + resp_doc + JSON.stringify($scope.moduleData.messagetocase));
        $http.get('http://193.182.16.151/SAME/uploadrelated.php?param1=' + $scope.moduleData.messagetocase.split("x")[1] + '&param2=' + resp_doc[0]).success(function(data) {
            // $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
        });
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);
})

.controller('CustomCalendarCtrl', function($scope, coreBOSWSAPI, coreBOSAPIStatus, $compile, uiCalendarConfig, $rootScope, Setup, $i18next, $window, $location) {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $scope.changeTo = 'Hungarian';
    /* event source that pulls from google.com */
    $scope.eventSource = {
        url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
        className: 'gcal-event', // an option!
        currentTimezone: 'America/Chicago' // an option!
    };
    /* event source that calls a function on every view switch */
    $scope.eventsF = function(element,start, end, timezone, callback) {
        var s = new Date(start).getTime() / 1000;
        var e = new Date(end).getTime() / 1000;
        var m = new Date(start).getMonth();
        var events = [{
            title: 'Feed Me ' + m,
            start: s + (50000),
            end: s + (100000),
            allDay: false,
            className: ['customFeed']
        }];
        callback(events);
    };
                 
    $scope.events = [];
    $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
    /* event source that contains custom events on the scope */
   
    coreBOSWSAPI.getCalendarVisits($scope.pickpay).then(function(response) {
        angular.forEach(response.data.result, function(value, key) {
            var event = {
                title: value.casesname,
                id: value.id,
                start: new Date(value.start[0], value.start[1], value.start[2], value.start[3], value.start[4], value.start[4]),
                end: new Date(value.end[0], value.end[1], value.end[2], value.end[3], value.end[4], value.end[4]),
                cases_no: value.cases_no,
                casesname: value.casesname,
                taskresult: value.taskresult,
                description: value.description,
                nome: value.nome,
                contact: value.contact,
                casesid:value.casesid
                // url:"#/portal_moduleview/Cases/44x" + value.casesid 
                   
               
            };
               
   
            $scope.events.push(event);
        });
    });
    $scope.events2 = [{
        title: 'Long Event',
        start: new Date(y, m, d - 5),
        end: new Date(y, m, d - 2)
    }];
    $scope.calEventsExt = {
        color: '#f00',
        textColor: 'yellow',
        events: [{
            type: 'party',
            title: 'Lunch',
            start: new Date(y, m, d, 12, 0),
            end: new Date(y, m, d, 14, 0),
            allDay: false
        }, {
            type: 'party',
            title: 'Lunch 2',
            start: new Date(y, m, d, 12, 0),
            end: new Date(y, m, d, 14, 0),
            allDay: false
        }, {
            type: 'party',
            title: 'Click for Google',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            url: 'http://google.com/'
        }]
    };
    /* alert on eventClick */
    $scope.alertOnEventClick = function(date, jsEvent, view) {
        $scope.alertMessage = (date.cases_no);
        $scope.casesno = (date.cases_no);
        $scope.casesname = (date.casesname);
        $scope.description = (date.description);
    };
    /* alert on Drop */
    $scope.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view) {
        revertFunc();
        //        if (!confirm("Are you sure about this change?")) {
        //            revertFunc();
        //        } else {
        //            coreBOSWSAPI.updateCalendarVisits(event.id, event.start.format(), event.end.format()).then(function(response) {});
        //        }
    };
    /* alert on Resize */
    $scope.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view) {
        $scope.alertMessage = ('Event Resized to make dayDelta ' + delta);
    };
    /* add and removes an event source of choice */
    $scope.addRemoveEventSource = function(sources, source) {
        var canAdd = 0;
        angular.forEach(sources, function(value, key) {
            if (sources[key] === source) {
                sources.splice(key, 1);
                canAdd = 1;
            }
        });
        if (canAdd === 0) {
            sources.push(source);
        }
    };
    /* add custom event*/
    $scope.addEvent = function() {
        $scope.events.push({
            title: 'Open Sesame',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            className: ['openSesame']
        });
    };
    /* remove event */
    $scope.remove = function(index) {
        $scope.events.splice(index, 1);
    };
    /* Change View */
    $scope.changeView = function(view, calendar) {
        uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
    };
    /* Change View */
    $scope.renderCalender = function(calendar) {
        if (uiCalendarConfig.calendars[calendar]) {
            uiCalendarConfig.calendars[calendar].fullCalendar('render');
        }
    };
    /* Render Tooltip tooltip: value.toolti,*/
    $scope.eventRender = function(event, element) {
        
          var newline="<br>";
          var result ="";
          if(event.taskresult!==''){
                result=i18n.t('Result') + ":  " + event.taskresult+ newline;
            }
          element.tooltip({
             html:true,
            'title': i18n.t('Cases No') + ":  " + event.cases_no +  newline
            +i18n.t('Description') + ":  " + event.description +newline
            +i18n.t('Technician') + ":  " + event.nome + newline
            +i18n.t('Contact Customer') + ":  " + event.contact+ newline
            +result

});
var previousTarget=null;
var DELAY = 700, clicks = 0, timer = null;

$(element).click(function() {
  clicks++;  //count clicks

        if(clicks !== 1) {

           clearTimeout(timer);    //prevent single-click action
            window.location.href = "#/portal_moduleview/Cases/44x"+event.casesid;
            clicks = 0;    //after action performed, reset counter
        }

});

    }
    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 450,
            editable: false,
            header: {
                //right: 'title',
                center: '',
                right: 'today prev,next'
            },
            defaultButtonText: {
		today: i18n.t("today")
            },
            // eventClick: $scope.alertOnEventClick,
//            eventMouseover: function(calEvent, domEvent) {
//                var layer = "<div id='events-layer' \n\
//                    \n\ style='position:absolute; width:200px; top:-90px; text-align:left; z-index:100;background-color:black;vertical-align:middle;'>\n\
//                    <b>" + i18n.t('Cases No') + ":</b> " + calEvent.cases_no + "<br/>\n\
//                    <b>" + i18n.t('Description') + ":</b> " + calEvent.description + "<br/>\n\
//                    <b>" + i18n.t('Technician') + ":</b> " + calEvent.nome + "<br/>\n\
//                    <b>" + i18n.t('Contact Customer') + ":</b> " + calEvent.contact + "</div>";
//                $(this).append(layer);
//            },
//            eventMouseout: function(calEvent, domEvent) {
//                $(this).find('div[id*=events-layer]').remove();
//            },
            eventDrop:   $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
        }
    };
    $scope.uiConfig.calendar.dayNames = [i18n.t("Sunday"), i18n.t("Monday"), i18n.t("Tuesday"), i18n.t("Wednesday"), i18n.t("Thursday"), i18n.t("Friday"), i18n.t("Saturday")];
    $scope.uiConfig.calendar.dayNamesShort = [i18n.t("Sun"), i18n.t("Mon"), i18n.t("Tue"), i18n.t("Wed"), i18n.t("Thur"), i18n.t("Fri"), i18n.t("Sat")];
    $scope.uiConfig.calendar.monthNames = [i18n.t("January"), i18n.t("February"), i18n.t("March"), i18n.t("April"), i18n.t("May"), i18n.t("June"), i18n.t("July"), i18n.t("August"), i18n.t("September"), i18n.t("October"), i18n.t("November"), i18n.t("December")];
    $scope.uiConfig.calendar.monthNamesShort = [i18n.t("Jan"), i18n.t("Feb"), i18n.t("Mar"), i18n.t("Apr"), i18n.t("May"), i18n.t("June"), i18n.t("July"), i18n.t("Aug"), i18n.t("Sept"), i18n.t("Oct"), i18n.t("Nov"), i18n.t("Dec")];

    $scope.changeLang = function() {
        if ($scope.changeTo === 'Hungarian') {
            $scope.uiConfig.calendar.dayNames = ["Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"];
            $scope.uiConfig.calendar.dayNamesShort = ["Vas", "Hét", "Kedd", "Sze", "Csüt", "Pén", "Szo"];
            $scope.changeTo = 'English';
        } else {
            $scope.uiConfig.calendar.dayNames = ["Domenica", "Lunedi", "Martedi", "Mercoledi", "Giovedi", "Venerdi", "Sabato"];
            $scope.uiConfig.calendar.dayNamesShort = ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"];
            $scope.changeTo = 'Hungarian';
        }
    };
    $scope.eventSources = [$scope.events];
})

.controller('navtop', function($scope, $rootScope, Setup,$filter, $i18next, $window, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.confirmpass='';
    $scope.newpass='';
    $scope.showlog={'log':false,'edit':false};
    coreBOSWSAPI.userInfo().then(function(response) {
        $scope.url = 'http://193.182.16.151/sameportal/app/';//Setup.corebosapi;
        $scope.user_data = response.data.result;
       // $scope.user_image = $scope.user_data.imagename;
    });
    $scope.contactid = coreBOSAPIStatus.getSessionInfo()._contactid;
    coreBOSWSAPI.doDescribe('Contacts').then(function(response) {
        $scope.modulefields = response.data.result.fields;
        var found = $filter('getArrayElementById')($scope.modulefields, 'cf_969', 'name');
        if (found != '' && found != undefined && found != null)
            $scope.opt = found.type.picklistValues;
        var found = $filter('getArrayElementById')($scope.modulefields, 'cf_972', 'name');
        if (found != '' && found != undefined && found != null){
            $scope.opt_cf_972 = found.type.picklistValues;
            $scope.opt_cf_972 = $scope.opt_cf_972.map(function(name, idx) {
                 var temp={
                     value:name.value,
                     label:i18n.t(name.value)
                 };
                 return temp;
            });
        }
    });
    $scope.retrieveContact = function (){
        coreBOSWSAPI.doQuery('select * from Contacts where id=' + $scope.contactid).then(function(response) {
            $scope.user_lastname = response.data.result[0]['lastname'];
            $scope.user_first_name = response.data.result[0]['firstname'];
            $scope.email_contact = response.data.result[0]['email'];
            $scope.lang_contact = response.data.result[0]['cf_969'];
            switch($scope.lang_contact) {
              case "de_de":
                 $scope.user_image = "images/german.jpg";
                 break;
              case "en_us":
                 $scope.user_image = "images/english.jpg";
                 break;
              case "es_es":
                 $scope.user_image = "images/spanish.png";
                 break;
              case "fr_fr":
                 $scope.user_image = "images/french.png";
                 break;
              case "it_it":
                 $scope.user_image = "images/italian.jpg";
                 break;
              case "nl_nl":
                 $scope.user_image = "images/dutch.jpg";
                 break;
              case "pt_br":
                 $scope.user_image = "images/portugal.png";
                 break;
              default:
                 $scope.user_image = "images/italian.jpg";
           }
            $scope.cont = {
                'title':response.data.result[0]['title'],
                'lastname':response.data.result[0]['lastname'],
                'firstname': response.data.result[0]['firstname'],
                'cf_969':{'value': response.data.result[0]['cf_969']},//language
                'cf_972':{'value': response.data.result[0]['cf_972']},//company
                'birthdate':response.data.result[0]['birthday'],
                'email':response.data.result[0]['email'],
                'cf_970': response.data.result[0]['cf_970'],//cel
                'cf_971': response.data.result[0]['cf_971'],//phone
                'phone': response.data.result[0]['phone'],
                'id':$scope.contactid
            };
        });
    };
    $scope.retrieveContact();
    
    $scope.reload_trans = function(){
        coreBOSWSAPI.getMenuStructure(coreBOSAPIStatus.getSessionInfo()._userid).then(function(response) {
            $scope.selectedmodule = [];
            if (response.data.result != undefined && response.data.result != '') {
                angular.forEach(response.data.result, function(value, key) {
                    var menu = {
                        title: key,
                        values: value
                    };
                    $scope.selectedmodule.push(menu);
                });
            } else {
                $scope.iscustomer = false;
                coreBOSWSAPI.doListTypes().then(function(response) {
                    $scope.listtypes = response.data.result;
                    var ltypes = coreBOSWSAPI.processListTypes(response.data.result.information, true, true);
                    $scope.selecttypes = ltypes;
                });
            }
            coreBOSWSAPI.getTranslationFile(coreBOSAPIStatus.getSessionInfo()._contactid,$scope.selectedmodule,true).then(function(response) {
                alert(i18n.t('Ok'));
            });
        });
        
    }
    $scope.change_pass = function(newpass,confirmpass){
        if(confirmpass!==newpass){
            alert('Password not matching');
        }
        else{
            coreBOSWSAPI.doChangePasswordPortal($scope.email_contact,confirmpass).then(function(response) {
                $scope.showlog.log=false;
            });
        }
    };
    $scope.edit_profile = function(cont){
        cont.cf_969=cont.cf_969.value;
        cont.cf_972=cont.cf_972.value;
        coreBOSWSAPI.doUpdate('Contacts', cont).then(function(response) {
            $scope.showlog.edit=false;
            coreBOSAPIStatus.setInvalidKeys(true);
            coreBOSAPIStatus.setSessionInfo({});
            //$location.path('/login');
            window.location.reload();
            //$rootScope.location = $location;
           }, function(response) {
           });
    };
})

.controller('logoutCtrl', function($scope, $location, coreBOSAPIStatus) {
    coreBOSAPIStatus.setInvalidKeys(true);
    coreBOSAPIStatus.setSessionInfo({});
    $location.path('/login');
})

.controller('termscCtrl', function($scope) {})

.controller('loginCtrl', function($scope, $http,$rootScope, $i18next, $filter, Setup, coreBOSWSAPI, coreBOSAPIStatus, $location) {
    if (!coreBOSAPIStatus.hasInvalidKeys()) {
        $location.path('/currmentorder');
    } else {
        $rootScope.menuShow = false;
        $scope.langs = [{
            name: i18n.t('English'),
            code: 'en'
        }, {
            name: i18n.t('Spanish'),
            code: 'es'
        }];
        $scope.sendemailreset = function() {
         var email=document.getElementById('search').value;
        //alert("called");
        $http.get('http://193.182.16.151/SAME/sendemailreset.php?to=' + email).error(function(data) {
        });
    };
        
        coreBOSWSAPI.setURL(Setup.corebosapi, false);
        $scope.$watch("username", function(newval, oldval) {
            coreBOSWSAPI.setcoreBOSUser(newval, false);
        });
        $scope.$watch("password", function(newval, oldval) {
            coreBOSWSAPI.setcoreBOSKey(newval, false);
        });
        $scope.login = function() {

            coreBOSWSAPI.doLoginPortal($scope.username, $scope.password).then(function(response) {
                coreBOSWSAPI.setConfigured();
                $rootScope.contactinfo = [{
                    contactid: '',
                    firstname: '',
                    lastname: '',
                    accountid: '',
                    email: '',
                }];
                $location.path('/portal_home');
            }, function() {
                coreBOSWSAPI.doLogin($scope.username, $scope.password).then(function() {
                    coreBOSWSAPI.setConfigured();
                    $location.path('/portal_home');
                }, function() {
                    coreBOSWSAPI.doLoginCustomerPortal($scope.username, $scope.password).then(function() {
                        coreBOSWSAPI.setConfigured();
                        coreBOSWSAPI.getMenuStructure(coreBOSAPIStatus.getSessionInfo()._userid).then(function(response) {
                            $scope.selectedmodule = [];
                            if (response.data.result != undefined && response.data.result != '') {
                                angular.forEach(response.data.result, function(value, key) {
                                    var menu = {
                                        title: key,
                                        values: value
                                    };
                                    $scope.selectedmodule.push(menu);
                                });
                            } else {
                                $scope.iscustomer = false;
                                coreBOSWSAPI.doListTypes().then(function(response) {
                                    $scope.listtypes = response.data.result;
                                    var ltypes = coreBOSWSAPI.processListTypes(response.data.result.information, true, true);
                                    $scope.selecttypes = ltypes;
                                });
                            }
                            
                            coreBOSWSAPI.getTranslationFile(coreBOSAPIStatus.getSessionInfo()._contactid,$scope.selectedmodule,false).then(function(response) {
                                i18n.init({
                                    fallbackLng: response.data.result
                                });
                                i18n.setLng(response.data.result, function(t) {
                                    $i18next.options.lng = response.data.result;
                                });
                                $location.path('/portal_home');
                            });
                        });
                        
                    }, function() {
                        $scope.prova = 'Not Correct';
                        $scope.ErrorUserNotValid = true;

                    });
                });
            });
        };
        $scope.cbAPIConfigured = coreBOSWSAPI;
        $scope.cbAPIKeys = coreBOSAPIStatus;
    }
})

.controller('HomeCtrl', function($scope, $i18next, $routeParams, $filter, coreBOSWSAPI, coreBOSAPIStatus, ngTableParams) {
    $scope.name_dashboard = 'Dealer Dashboard';
    $scope.show_inline = false;
    $scope.show_list = true;
    $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
    $scope.put_inline = function(relmod, module_trans) {
        coreBOSWSAPI.doGetUi10RelatedRecords($scope.pickpay, 'PickAndPay', relmod, 'CUSTOMERPORTAL').then(function(response) {
            $scope.graphRecords = response.data.result.records;
            $scope.sub_proj = response.data.result.sub_proj;
            $scope.month_proj = response.data.result.month_proj;
            $scope.pick_month_proj = response.data.result.pick_month_proj;
            $scope.module_trans = module_trans;
            $scope.type = response.data.result.type;
            $scope.headers = response.data.result.headers;
            $scope.fields = response.data.result.fields;
            $scope.optionsgraphRecords = {
                chart: {
                    type: 'multiBarChart',
                    height: 450,
                    "width": 685,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 60,
                        left: 65
                    },
                    clipEdge: false,
                    staggerLabels: false,
                    transitionDuration: 500,
                    stacked: false,
                    tooltips: false,
                    xAxis: {
                        axisLabel: module_trans,
                        showMaxMin: false,
                        tickFormat: function(d) {
                            return d;
                        }
                    },
                    yAxis: {
                        axisLabel: '',
                        axisLabelDistance: 20,
                        tickFormat: function(d) {
                            return d;
                        }
                    }
                }
            };
            $scope.show_inline = true;
            $scope.show_list = false;
        });
    };
    $scope.cancel = function() {
        $scope.show_inline = false;
        $scope.show_list = true;
    };
    coreBOSWSAPI.doQuery('select dealerole,accountname from Accounts where id=' + $scope.pickpay).then(function(response) {
        $scope.dealerole = response.data.result[0]['dealerole'];
        $scope.accountname = response.data.result[0]['accountname'];
        coreBOSWSAPI.doQuery('select dealername from Dealer where id=' + $scope.dealerole).then(function(response) {
            $scope.dealername = response.data.result[0]['dealername'];
        });
    });
    //    coreBOSWSAPI.getRelatedModules('PickAndPay', 'CUSTOMERPORTAL').then(function(response) {
    //      $scope.widgets = [];
    //      angular.forEach(response.data.result, function(value, key) {
    //        var relmod = value.module;
    //        var module_trans = value.module_trans;
    //        var widget = {};
    //        widget.module = relmod;
    //        widget.module_trans = module_trans;
    //        $scope.widgets.push(widget);
    //        $scope.put_inline(relmod, module_trans);
    //      });
    //    });

    coreBOSWSAPI.getHomeWidgets($scope.pickpay).then(function(response) {
        $scope.records_Messages = response.data.result.records_Messages;
        $scope.headers_Messages = response.data.result.headers_Messages;
        $scope.fields_Messages = response.data.result.fields_Messages;
        $scope.records_Cases = response.data.result.records_Cases;
        $scope.headers_Cases = response.data.result.headers_Cases;
        $scope.fields_Cases = response.data.result.fields_Cases;
        $scope.nr_waiting = response.data.result.nr_waiting;
        $scope.nr_assigned = response.data.result.nr_assigned;
        $scope.nr_tobe = response.data.result.nr_tobe;
        $scope.nr_resolved = response.data.result.nr_resolved;
        $scope.assigned_data = response.data.result.assigned_data;
        $scope.tobe_data = response.data.result.tobe_data;
        $scope.resolved_data = response.data.result.resolved_data;

        $scope.tableParamsMessages = new ngTableParams({
            page: 1, // show first page
            count: 5 // count per page
        }, {
            total: $scope.records_Messages.length, // length of data
            counts: [], // hide page counts control
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')($scope.records_Messages, params.filter()) :
                    $scope.records_Messages;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.records_Messages;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.tableParamsCases = new ngTableParams({
            page: 1, // show first page
            count: 5 // count per page
        }, {
            total: $scope.records_Cases.length, // length of data
            counts: [], // hide page counts control
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')($scope.records_Cases, params.filter()) :
                    $scope.records_Cases;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.records_Cases;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.tableParamsCases_assigned_data = new ngTableParams({
            page: 1, // show first page
            count: 5 // count per page
        }, {
            total: $scope.assigned_data.length, // length of data
            counts: [], // hide page counts control
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')($scope.assigned_data, params.filter()) :
                    $scope.assigned_data;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.assigned_data;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.tableParamsCases_tobe_data = new ngTableParams({
            page: 1, // show first page
            count: 5 // count per page
        }, {
            total: $scope.tobe_data.length, // length of data
            counts: [], // hide page counts control
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')($scope.tobe_data, params.filter()) :
                    $scope.tobe_data;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.tobe_data;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.tableParamsCases_resolved_data= new ngTableParams({
            page: 1, // show first page
            count: 5 // count per page
        }, {
            total: $scope.resolved_data.length, // length of data
            counts: [], // hide page counts control
            getData: function($defer, params) {
                var filteredData = params.filter() ?
                    $filter('filter')($scope.resolved_data, params.filter()) :
                    $scope.resolved_data;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.resolved_data;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.generateColumns = function(sampleData, type) {
            var colNames = sampleData;
            var cols = colNames.map(function(name, idx) {
                var filter = {};
                if (type === 'mess') {
                    var label = $scope.headers_Messages[idx];
                } else {
                    var label = $scope.headers_Cases[idx];
                }
                var label_trans = i18n.t(label);
                var returned_arr = {
                    title: label_trans,
                    sortable: name,
                    show: true,
                    field: name
                };
                filter[name] = 'text';
                returned_arr['filter'] = filter;
                return returned_arr;
            });
            return cols;
        };

        $scope.followRecord_mess = function(row) {
            window.location = "#/portal_moduleview/Cases/" + row.casesid;
        };
        $scope.followRecord_case = function(row) {
            window.location = "#/portal_moduleview/Cases/" + row.id;
        };
        $scope.cols_Message = $scope.generateColumns($scope.fields_Messages, 'mess');
        $scope.cols_Cases = $scope.generateColumns($scope.fields_Cases, 'cas');
    });
})

.controller('StockMonitor', function($scope, $i18next, $routeParams, $filter, $window, coreBOSWSAPI, coreBOSAPIStatus, ngTableParams) {
    $scope.show_results = false;
    $scope.fldValues = {};
    $scope.fldValuesParam = {};
    $scope.enable_action = false;
    $scope.filter_val = '';
    $scope.module = $routeParams.name;
    $scope.moduleLabel = $routeParams.nameLabel;
    $scope.cerca_name = 'Cerca';
    $scope.enable_cerca = false;
    $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
    coreBOSWSAPI.doGetDashboardInstance($scope.module).then(function(response) {
        $scope.name_dashboard = $scope.moduleLabel;
        $scope.structure = response.data.result;
        $scope.fldValues[167] = {
            'filter_project': "282"
        };
    });
    $scope.findResults = function(blockid, values) {
        $scope.searchText = '';
        $scope.blc = blockid;
        $scope.vls = values;
        if ($scope.tableParams) {
            $scope.show_results = false;
            $scope.cerca_name = 'Ricerca';
            $scope.enable_cerca = true;
            $scope.tableParams.reload();
        } else {
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 5 // count per page
            }, {
                counts: [], // hide page counts control
                getData: function($defer, params) {
                    coreBOSWSAPI.doRetrieveResults($scope.module, $scope.blc, $scope.vls, $scope.pickpay).then(function(response) {
                        if (response.data.result.records != null) {
                            $scope.returnedResults = response.data.result.records;
                        } else {
                            $scope.returnedResults = [];
                        }
                        $scope.tot_length = $scope.returnedResults.length;
                        $scope.tot = response.data.result.total;
                        $scope.columns = response.data.result.columns;
                        $scope.fieldlabels = response.data.result.fieldlabels;
                        angular.forEach($scope.columns, function(value, key) {
                            if (value === 'descrizionearticolo' || value === 'descrizionearticolo_2') {
                                $scope.columns.splice(key, 1);
                                $scope.fieldlabels.splice(key, 1);
                            }
                        });
                        $scope.blockParameters = response.data.result.blockParameters;
                        var orderedData = params.filter ?
                            $filter('filter')($scope.returnedResults, params.filter()) : $scope.returnedResults;

                        params.total($scope.tot_length); // set total for recalc pagination
                        $defer.resolve($scope.users = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    });
                }
            });
        }
        $scope.show_results = true;
        $scope.cerca_name = 'Cerca';
        $scope.enable_cerca = false;
        $scope.getTitle = function(val) {
            return $scope.fieldlabels[val];
        };
        $scope.checkboxes = {
            'checked': false,
            items: {}
        };
        $scope.allselected = false;
        $scope.selectAll = function() {
            angular.forEach($scope.returnedResults, function(value, key) {
                if ($scope.allselected) {
                    $scope.checkboxes.items[value.recordid] = false;
                } else {
                    $scope.checkboxes.items[value.recordid] = true;
                }
            });
            $scope.allselected = !$scope.allselected;
        };
        $scope.$watch('checkboxes.items', function(values) {
            if (!$scope.users) {
                return;
            }
            var checked = 0,
                unchecked = 0,
                total = $scope.users.length;
            angular.forEach($scope.users, function(item) {
                checked += ($scope.checkboxes.items[item.recordid]) || 0;
                unchecked += (!$scope.checkboxes.items[item.recordid]) || 0;
            });
            if ((unchecked == 0) || (checked == 0)) {
                $scope.checkboxes.checked = (checked == total);
            }
            angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
        }, true);
    };
    $scope.runAction = function(actionid, values, selectedids) {
        var arr_sel = '';
        $scope.enable_action = true;
        $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
        var count = 0;
        angular.forEach(selectedids, function(value, key) {
            if (value == true) {
                arr_sel += (count === 0 ? key : ',' + key);
                count++;
            }

        });
        var senddata = {
            'recordid': arr_sel,
            'actionid': actionid,
            'pickpay': $scope.pickpay
        };
        if (values != undefined) {
            values = angular.extend(values, senddata);
        } else {
            values = senddata;
        }
        coreBOSWSAPI.doRunAction(values).then(function(response) {
            $scope.returnedResults = response.data.result;
            if ($scope.returnedResults.outputtype == 'Link') {
                var url = $scope.returnedResults.pdfURL;
                $window.open(url, "_blank"); //_self
            } else if ($scope.returnedResults.outputtype == 'Alert') {
                alert($scope.returnedResults.alert);
            } else {
                alert("Eseguito con successo");
            }
            $scope.fldValuesParam = {};
            $scope.tableParams.reload();
            $scope.enable_action = false;
        });
    };
})

.controller('moduleCtrl', function($scope, $i18next, $routeParams, $window, $filter, coreBOSWSAPI, coreBOSAPIStatus, ngTableParams) {
    $scope.currentLang = $i18next.options.lng;
    $scope.myPageItemsCount = 0;
    $scope.myItemsTotalCount = 0;
    $scope.moduleList = [];
    $scope.module = $routeParams.module;
    $scope.moduleLabel = $routeParams.moduleLabel;
    $scope.modulefields = [];
    $scope.fldValuesParam = {};
    var filter_col = '';
//    coreBOSWSAPI.getRelatedActions($scope.module, 'LISTVIEWPORTAL').then(function(response) {
//        $scope.relatedactions = response.data.result;
//    });
    coreBOSWSAPI.doDescribe($scope.module).then(function(response) {
        $scope.idPrefix = response.data.result.idPrefix;
        $scope.createable = response.data.result.createable;
        $scope.updateable = response.data.result.updateable;
        $scope.modulefields = response.data.result.fields;
        $scope.getModuleType = function(field) {
            var ret = '';
            var found = $filter('getArrayElementById')($scope.modulefields, field, 'name');
            if (found != '' && found != undefined && found != null)
                ret = found.type.name;
            return ret;
        };
        $scope.getModuleLabel = function(field, modulefields) {
            var ret = '';
            var found = $filter('getArrayElementById')(modulefields, field, 'name');
            if (found != '' && found != undefined && found != null)
                ret = found.label;
            return ret;
        };
        $routeParams.module = $routeParams.module.toLowerCase();
        $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
        var rel_mod='Products';
        var name='cf_936';
        $scope.lbl_936='';
        coreBOSWSAPI.doDescribe(rel_mod).then(function(responserel) {
            $scope.rel_modulefields = responserel.data.result.fields;
            $scope.lbl_936=$scope.getModuleLabel(name,$scope.rel_modulefields);
        });
        coreBOSWSAPI.getFilterFields($scope.module).then(function(response) {
            $scope.fields = response.data.result.fields;
            $scope.linkfields = response.data.result.linkfields;
            // console.log("useriiloguar"+coreBOSAPIStatus.getSessionInfo()._userid);
            $scope.useriloguar = '';//coreBOSAPIStatus.getSessionInfo()._contactid;
            coreBOSWSAPI.doGetCustomerTypes($scope.module, $scope.fields, $scope.pickpay, $scope.useriloguar).then(function(response) {
                //$scope.query_module=response.data.result[$routeParams.module]['record_set'];
                // console.log("parametratthires"+$scope.module+' '+$scope.fields+' '+$scope.pickpay+' '+$scope.useriloguar.split("x")[1]);
                if(response.data.result.length>1)
                     $scope.moduleList = response.data.result[$scope.module]['record_set'];
                if($scope.moduleList.length<1){
                    coreBOSWSAPI.doQuery('select * from '+$routeParams.module).then(function(response) {
                            $scope.moduleList = response.data.result;
                    
                //$scope.labels = response.data.result[$scope.module]['labels'];
                // console.log("the labels"+JSON.stringify($scope.labels));
                $scope.myPageItemsCount = $scope.moduleList.length;
                $scope.checkboxes = {
                    'checked': false,
                    items: {}
                };


                $scope.allselected = false;
                $scope.selectAll = function() {
                    angular.forEach($scope.moduleList, function(value, key) {
                        if ($scope.allselected) {
                            $scope.checkboxes.items[value.id] = false;
                        } else {
                            $scope.checkboxes.items[value.id] = true;
                        }
                    });
                    $scope.allselected = !$scope.allselected;
                };

                $scope.$watch('checkboxes.items', function(values) {
                    if (!$scope.users) {
                        return;
                    }
                    var checked = 0,
                        unchecked = 0,
                        total = $scope.moduleList.length;
                    angular.forEach($scope.moduleList, function(item) {
                        checked += ($scope.checkboxes.items[item]) || 0;
                        unchecked += (!$scope.checkboxes.items[item]) || 0;
                    });
                    if ((unchecked == 0) || (checked == 0)) {
                        $scope.checkboxes.checked = (checked == total);
                    }
                    angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
                }, true);
                $scope.runAction = function(actionid, values, selectedids) {
                    var arr_sel = '';
                    $scope.enable_action = true;
                    $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
                    var count = 0;
                    angular.forEach(selectedids, function(value, key) {
                        if (value == true) {
                            var v = key.split('x');
                            arr_sel += (count === 0 ? v[1] : ',' + v[1]);
                            count++;
                        }
                    });
                    var senddata = {
                        'recordid': arr_sel,
                        'actionid': actionid,
                        'pickpay': $scope.pickpay
                    };
                    var val_arr = values.split(',');
                    senddata = angular.extend({
                        'fileid': val_arr[0],
                        'entityid': val_arr[1]
                    }, senddata);
                    coreBOSWSAPI.doRunAction(senddata).then(function(response) {
                        $scope.returnedResults = response.data.result;
                        if ($scope.returnedResults.outputtype == 'Link') {
                            var url = $scope.returnedResults.pdfURL;
                            $window.open(url, "_blank"); //_self
                        } else if ($scope.returnedResults.outputtype == 'Alert') {
                            alert($scope.returnedResults.alert);
                        } else if ($scope.returnedResults.outputtype == '') {} else {
                            alert("Eseguito con successo");
                        }
                    });
                    $scope.enable_action = false;
                };
                $scope.searchText = '';
                $scope.myPageItemsCount = $scope.moduleList.length;
                $scope.filter_fld = 'cf_902';
                $scope.opt = [];
                
                $scope.generateColumns = function(sampleData) {
                    var colNames = sampleData;//Object.getOwnPropertyNames(sampleData);
                    
                    var cols = colNames.map(function(name, idx) {
                        var filter = {};
                        var label='';
                        var shown=true;                        
                        label=$scope.getModuleLabel(name,$scope.modulefields);
                        var label_trans = i18n.t(label);//$scope.labels[idx]; //
                        var returned_arr = {
                            title: label_trans,
                            sortable: name,
                            show: shown,
                            field: name
                        };
                        var found = $filter('getArrayElementById')($scope.modulefields, name, 'name');
                        if (found != null)
                            if (found.type.name == 'picklist') {
                                filter[name] = 'select'; //'select';
                                returned_arr['filter'] = filter;
                                var val_pick = [];
                                var val_pick2 = [];
                                val_pick.push({
                                    id: '',
                                    title: ''
                                });
                                val_pick2.push('');
                                angular.forEach(found.type.picklistValues, function(value, key) {
                                    val_pick.push({
                                        id: value.value,
                                        title: i18n.t(value.value)
                                    });
                                    val_pick2.push(value.value);
                                });
                                returned_arr['filterData']=val_pick;
                            } else {
                                filter[name] = 'text';
                                returned_arr['filter'] = filter;
                            }

                        return returned_arr;
                    });
//                    var idCol = _.findWhere(cols, {
//                        field: $scope.fields[0]
//                    });
                    return cols;//[idCol].concat(_.without(cols, idCol));
                };

                $scope.cols = $scope.generateColumns($scope.fields);//$scope.moduleList[0]

//                for (var index = $scope.cols.length - 1; index >= 0; index--) {
//
//                    if (!(_.contains($scope.labels, $scope.cols[index].title))) {
//                        $scope.cols.splice(index, 1);
//                    };
//                }
                // Would using a filter be faster...
                // $scope.cols = $scope.cols.filter(function(index){
                //     return $scope.cols[index].title == undefined;
                // });
                $scope.searchText = {
                    $: ''
                };
                $scope.tableParams = new ngTableParams({
                    page: 1, // show first page
                    count: 10, // count per page
                    filter: $scope.searchText
                }, {
                    filterDelay: 0,
                    getData: function($defer, params) {
                        var filteredData = params.filter() ?
                            $filter('filter')($scope.moduleList, params.filter()) :
                            $scope.moduleList;
                        var orderedData = params.sorting() ?
                            $filter('orderBy')(filteredData, params.orderBy()) :
                            $scope.moduleList;
                        params.total(orderedData.length); // set total for recalc pagination
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                    }
                });
                $scope.$watch("searchText.$", function() {
                    $scope.tableParams.reload();
                    $scope.tableParams.page(1); //Add this to go to the first page in the new pagging
                });
                $scope.followRecord = function(row) {
                    window.location = "#/portal_moduleview/" + $scope.module + "/" + row.id;
                };


                $scope.get_contact = function() {
                    $scope.tableParams.reload();
                };
                if ($scope.module == 'Vitals') {
                    less.modifyVars({
                        '@pageRightWidth': '60%',
                        '@pageLeftWidth': '37%'
                    });
                } else {
                    less.modifyVars({
                        '@pageRightWidth': '27%',
                        '@pageLeftWidth': '70%'
                    });
                }
                });
                }//doQuery select *
            });//get customer types
        });
    });
})

.controller('CreateViewCtrl', function($scope, $i18next, $http, $routeParams,Setup, ngTableParams, $filter, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.modulefieldList = [{
        field: '',
        val: ''
    }];
    $scope.todaydate=new Date();
    $scope.todaydate.setDate($scope.todaydate.getDate() + 1);
    $scope.module = $routeParams.module;
    $scope.moduleData = {};
    $scope.entity = {};
    $scope.creating = false;
    $scope.iscreateview = true;
    $scope.type10referals = {};
    $scope.ui10Readable = {};
    $scope.uiEvoReadable = [];
    $scope.uiEvoValue = [];
    $scope.usersList = {};
    $scope.groupList = {};
    $scope.type10list = {};
    $scope.hidden = true;

    $scope.chipSelectedItem = null;
    $scope.chipOptions = {};
    $scope.chipSelectedOptions = {}
    $scope.chipNumberChips = [];
    $scope.chipNumberChips2 = [];
    $scope.chipNumberBuffer = '';

    $scope.moduleEditable = {};
    $scope.moduleMandatory = {};
    $scope.mandatory_fields = {};

    $scope.formatedDates = {};

    $scope.getUsers = function() {
        coreBOSWSAPI.doQuery("SELECT id, first_name,last_name FROM Users").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.last_name + " " + value.first_name
                });
            });
            $scope.usersList = item;
        });
    }
    $scope.getGroups = function() {
        coreBOSWSAPI.doQuery("SELECT id, groupname FROM Groups").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.groupname
                });
            });
            $scope.groupList = item;
        });
    };


    $scope.autofill = function(idvin) {
        coreBOSWSAPI.getAutocompletefields(idvin).then(function(response) {
                var tempItem = response.data.result[0];
                $scope.thisRef = tempItem.productname;
                $scope.ui10Readable['productcase'] = tempItem.productname;
                $scope.moduleData['productcase'] = "14x" + tempItem.productvin;
                $scope.moduleData['cf_998'] = tempItem.cf_982;
                $scope.ui10Readable['cf_998'] = tempItem.productname;

                // console.log("RESPONSE", tempItem);
                // $scope.cf_998 = response.data.result[0].productvin;
                // $scope.productcase = response.data.result[0].productvin;
                // $scope.productcase_display = response.data.result[0].productname;
            }, function(response) {
                // console.log("RESPONSE", response);
            }

        );
    }

    $scope.ownerUpdated = function(item, model) {
        $scope.thisOwner = item.name;
        $scope.moduleData["assigned_user_id"] = item.id;
        $scope.debounceSaveUpdates("assigned_user_id", item.id);

    }
    $scope.refUpdated = function(item, model, label) {
        $scope.thisRef = item.name;
        $scope.ui10Readable[label] = item.name;
        $scope.moduleData[label] = item.id;

    }
    $scope.getUsers();
    $scope.getGroups();


    $scope.adjustDateS = function(label, value) {
        $scope.moduleData[label] = moment(value).format('YYYY-MM-DD');
    };
    
    $scope.changedate = function(date){
    alert(date);
  };
    $scope.$watch("formatedDates", function(values) {
        $scope.moduleData['cf_908']=moment($scope.formatedDates['cf_908']).format('YYYY-MM-DD');
        console.log($scope.formatedDates);
        angular.forEach($scope.formatedDates, function(value, key) {
            alert(value+' '+key);
        });
    });

    $scope.doCancel = function() {
        $location.url("/portal_module/" + $scope.module + "/" + $scope.module);
    };

    coreBOSWSAPI.getFieldDep($routeParams.module, 'FieldDependencyPortal').then(function(response) {
        $scope.map_field_dep = response.data.result.all_field_dep;
        $scope.MAP_RESPONSIBILE_FIELDS = response.data.result.MAP_RESPONSIBILE_FIELDS;
        $scope.MAP_RESPONSIBILE_FIELDS3 = response.data.result.MAP_RESPONSIBILE_FIELDS3;
        $scope.MAP_RESPONSIBILE_FIELDS2 = response.data.result.MAP_RESPONSIBILE_FIELDS2;
        $scope.MAP_PCKLIST_TARGET = response.data.result.MAP_PCKLIST_TARGET;
        
    });
    $scope.showLogicRow = function(row) {
        var ret = false;
        row.forEach(function(entry) {
            if ($scope.showLogic(entry)) {
                ret = true;
            }
        });
        return ret;
    };
    $scope.showLogic = function(field) {
        var ret = true;
        var fieldname = '';
        angular.forEach($scope.map_field_dep, function(value, key) {
            var conditionResp = '';
            if (value.targetfield.indexOf(field) != -1) {
                angular.forEach(value.respfield, function(resp_val, resp_val_key) {
                    var resp_value = value.respvalue_portal[resp_val_key];
                    var comparison = value.comparison[resp_val_key];
                    if (resp_val_key !== 0) {
                        if (comparison === 'empty' || comparison === 'notempty')
                            conditionResp += ' || ';
                        else
                            conditionResp += ' && ';
                    }
                    if (comparison === 'empty')
                        conditionResp += $scope.moduleData[resp_val] === '' || $scope.moduleData[resp_val] === undefined;
                    else if (comparison === 'notempty')
                        conditionResp += $scope.moduleData[resp_val] !== '' && $scope.moduleData[resp_val] !== undefined;
                    else {
                        conditionResp += resp_value.indexOf($scope.moduleData[resp_val]) != -1;
                    }
                });

                angular.forEach(value.targetfield, function(target_fld, target_fld_key) {
                    if (field == target_fld && value.action[target_fld_key] == 'hide' && eval(conditionResp)) {
                        ret = false;
                        fieldname = field;
                    } else if (field == target_fld && value.action[target_fld_key] == 'show' && eval(conditionResp)) {
                        ret = true;
                        fieldname = field;
                    } else if (field == target_fld && fieldname != field && value.action[target_fld_key] == 'hide') {
                        ret = false;
                        fieldname = field;
                    }
                    if (value.automatic[target_fld_key] !== '') {
                        if(eval(conditionResp) )
                        {
                            $scope.moduleData[target_fld]=value.automatic[target_fld_key];
                        }
                    }
                });
            }
        });
        return ret;
    };
    
    $scope.mandatoryLogic = function() {
        var ret = true;
        var fieldname = '';
        var blocks = [];
        angular.forEach($scope.map_field_dep, function(value, key) {
            var keepGoing = true;
            angular.forEach(value.targetfield, function(target_fld, target_fld_key) {
                if (keepGoing) {
                    var conditionResp = '';
                    angular.forEach(value.respfield, function(resp_val, resp_val_key) {
                        var resp_value = value.respvalue_portal[resp_val_key];
                        var comparison = value.comparison[resp_val_key];
                        if (resp_val_key !== 0) {
                            if (comparison === 'empty' || comparison === 'notempty')
                                conditionResp += ' || ';
                            else
                                conditionResp += ' && ';
                        }
                        if (comparison === 'empty')
                            conditionResp += ($scope.moduleData[resp_val] === '' || $scope.moduleData[resp_val] === undefined);
                        else if (comparison === 'notempty')
                            conditionResp += ($scope.moduleData[resp_val] !== '' && $scope.moduleData[resp_val] !== undefined);
                        else {
                            conditionResp += resp_value.indexOf($scope.moduleData[resp_val]) != -1;
                        }
                    });
                    if (value.mandatory[target_fld_key] === 'mandatory' && eval(conditionResp) && $scope.moduleData[target_fld] == undefined) {
                        ret = false;
                        // console.log('mapid ' + key);
                        var mand = target_fld + '_mandText';
                        $scope[mand] = true;
                        var field_data = {
                            name: target_fld
                        };
                        blocks.push(field_data);
                    }
                }
            });
        });
        $scope.mandatory_fields = blocks;
        return ret;
    };

    $scope.getMandatoryText = function(fld) {
        var mand = fld + '_mandText';
        var text = '';
        if ($scope[mand]) {
            text = $scope.getModuleLabel(fld, $scope.modulefields) + ' is mandatory';
        }
        for (var i = $scope.mandatory_fields.length - 1; i >= 0; i--) {
            var fld_mand = $scope.mandatory_fields[i].name;
            var temp_mand = fld_mand + '_mandText';
            if ($scope.moduleData[fld_mand] !== undefined) {
                $scope.mandatory_fields.splice(i, 1);
                $scope[temp_mand] = false;
            }
        }
        return text;
    };
    

    $scope.getUi10Readable = function(module, curr_ui10, label) {
        $scope.ui10Readable[label] = "Empty";
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module + ' where id=' + curr_ui10).then(function(response2) {
                var value;
                if (module != "Contacts") {
                    value = response2.data.result[Object.keys(response2.data.result)[0]][response.data.result.labelFields];
                } else {
                    value = response2.data.result[Object.keys(response2.data.result)[0]];
                    value = value.firstname + " " + value.lastname;
                }
                $scope.ui10Readable[label] = value;
            });
        });
    };
    
    
    $scope.getUi10List = function(fld,moduleName) {

        $scope.profamname = {};
        $scope.prodfvname = {};
        $scope.brandname  = {};
        $scope.comfamily  = {};
        $scope.productvin={};
        var pageItems=5;
        coreBOSWSAPI.doDescribe(moduleName).then(function(response) {
            var tempfields,tempfields_col;
            tempfields = response.data.result.labelFields;
            if (moduleName != "Products") {
                tempfields_col = response.data.result.labelFields;
            } else {
                tempfields_col = response.data.result.labelFields + ", productfamilycatalog";
            }
            $scope.searchText = {
                    $: ''
                };
                
            $scope.$watch("searchText.$", function() {
                $scope[name].reload();
                $scope[name].page(1); //Add this to go to the first page in the new pagging
            });
            var name='tableParams'+fld;
            $scope[name] = new ngTableParams({
                page: 1,            // show first page
                count: 5  // count per page

            }, {
               counts: [], 
                getData: function($defer, params) {
                    var jsonFilter={};
                    jsonFilter[tempfields]=$scope.searchText.$;
                    var filterByFields=jsonFilter;
                    var currentPage=params.page()-1;
                    var where = coreBOSWSAPI.getWhereCondition(null,null, filterByFields, tempfields, false);
                    var limit = coreBOSWSAPI.getLimit(pageItems,currentPage*pageItems);
                    var query = 'SELECT ' + tempfields_col + ' FROM ' + moduleName + where + limit;
                    var querytotal = 'SELECT count(*) FROM ' + moduleName + where;
                    coreBOSWSAPI.doQuery(querytotal).then(function(response) {
                            $scope.myItemsTotalCount = response.data.result[0].count;
                    });
                    coreBOSWSAPI.doQuery(query).then(function(response2) {
                    var item = [];
                    var thisModuleData = [];
                    angular.forEach(response2.data.result, function(value, key) {
                            if (moduleName == 'ProductDetail') {
                                coreBOSWSAPI.getproductfamily(value.id).then(function(response) {                        
                                    $scope.prodfvname[value.id] = response.data.result[0].productfamilyname;
                                    $scope.productvin[value.id] =response.data.result[0].productname;
                                    // alert (response.data.result[0].productvin);
                                })
                            }; 
                            if (moduleName == "Contacts") {
                                item.id = value.id;
                                item.name = value.firstname + " " + value.lastname;
                            }
                            if (moduleName=='Products') {
                                item.family = value.productfamilycatalog;
                                coreBOSWSAPI.doQuery('SELECT productfamilyname FROM ProductFamily where id=' + item.family).then(function(response) {
                                    $scope.profamname[response.data.result[0].id] = response.data.result[0].productfamilyname;
                                });
                                coreBOSWSAPI.doQuery('SELECT cf_935,cf_936  FROM Products where id=' + item.id).then(function(response) {
                                    $scope.brandname[response.data.result[0].id] = response.data.result[0].cf_935;
                                    $scope.comfamily[response.data.result[0].id] = response.data.result[0].cf_936;
                                });
                            }
                        thisModuleData.push({
                            name: value[tempfields],
                            id: value['id']
                        });
                    });
                    //$scope.type10list[moduleName] = thisModuleData;
                    $scope.optionList=thisModuleData;
                    var orderedData = $scope.optionList;
                      params.total($scope.myItemsTotalCount);
                      if (currentPage == 0) {
                            params.settings().startItemNumber = 1;
                        }
                        else {
                            params.settings().startItemNumber = currentPage * params.settings().countSelected + 1;
                        }
                        params.settings().endItemNumber = params.settings().startItemNumber +$scope.myItemsTotalCount - 1;
                      $defer.resolve(orderedData);
                });
                }
            });
        });
    }
    $scope.getUiEvoReadable = function(curr_ui10, label) {
        $scope.uiEvoReadable[label] = [];
        $scope.uiEvoValue[label] = [];
//        coreBOSWSAPI.doDescribe(module).then(function(response) {
//            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module + ' where id=' + curr_ui10).then(function(response2) {
//                var value;
//                if (module != "Contacts") {
//                    value = response2.data.result[Object.keys(response2.data.result)[0]][response.data.result.labelFields];
//                } else {
//                    value = response2.data.result[Object.keys(response2.data.result)[0]];
//                    value = value.firstname + " " + value.lastname;
//                }
//                $scope.uiEvoReadable[label] = value;
//            });
//        });
    };
    $scope.getUiEvoList = function(fld) {
        $scope.profamname = {};
        $scope.prodfvname = {};
        $scope.brandname  = {};
        $scope.comfamily  = {};
        $scope.productvin={};
        var pageItems=5;
        $scope.searchTextEvo = {
                $: ''
            };                
        $scope.$watch("searchTextEvo.$", function() {
            $scope[name].reload();
            $scope[name].page(1); //Add this to go to the first page in the new pagging
        });
        var name='tableParamsEvo'+fld;
        $scope[name] = new ngTableParams({
            page: 1,            // show first page
            count: 5  // count per page

        }, {
           counts: [], 
            getData: function($defer, params) {
                var term=$scope.searchTextEvo.$;
                var currentPage=params.page()-1;
                var limit = coreBOSWSAPI.getLimit(pageItems,currentPage*pageItems);
                coreBOSWSAPI.getEvoReferenceAutocomplete(term,'contains',limit,fld).then(function(response2) {
                var item = [];
                var thisModuleData = [];
                $scope.myItemsTotalCount=response2.data.result[0]['totalRec'];
                angular.forEach(response2.data.result, function(value, key) {                            
                    thisModuleData.push({
                        crmname: value['crmname'],
                        crmid: value['crmid']
                    });
                });
                $scope.optionList=thisModuleData;
                var orderedData = $scope.optionList;
                  params.total($scope.myItemsTotalCount);
                  if (currentPage == 0) {
                        params.settings().startItemNumber = 1;
                    }
                    else {
                        params.settings().startItemNumber = currentPage * params.settings().countSelected + 1;
                    }
                    params.settings().endItemNumber = params.settings().startItemNumber +$scope.myItemsTotalCount - 1;
                  $defer.resolve(orderedData);
            });
            }
        });
    }
     $scope.refEvoUpdated = function(item, model, label) {
        var temp=item.crmid.split('x');
        var idx = $scope.uiEvoValue[label].indexOf(temp[1]);
        if (idx === -1) {
            $scope.uiEvoReadable[label].push(item.crmname);
            $scope.uiEvoValue[label].push(temp[1]);
            if ($scope.uiEvoReadable[label].length > 1) {
                $scope.moduleData[label] = $scope.uiEvoValue[label].join(",");
            }
        } 
    }
    $scope.getModuleValue = function(field, moduleData, modulefields) {
        var type = '',
            value;
        type = $scope.getModuleType(field, modulefields);
        if (type == 'boolean') {
            value = ((moduleData[field] == '0' || moduleData[field] == null || moduleData[field] == '') ? 'No' : 'Si');
        } else if (type == 'reference') {
            if (moduleData[field] != undefined) {
                var temp_ui10 = field + '_tempui10';
                value = moduleData[field].value10;
                var temp_id = field + '_autocomid';
                $scope[temp_id] = moduleData[field].id10;
                if (value == '' || value == null) {
                    value = $scope[temp_ui10];
                }
                $scope[temp_ui10] = value;
            } else {
                value = '';
            }
        } else if (type == 'date' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy');
        } else if (type == 'datetime' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy HH:mm:ss');
        } else if (type == 'multipicklist') {
            var modVal = moduleData[field];
            var v2 = modVal;
            var v = '';
            if (modVal.indexOf('|##|') != -1) {
                v = modVal.split('|##|');
                v2 = v.join(',');
            } else if (angular.isArray(modVal)) {
                v = modVal;
                v2 = modVal.join(',');
            } else if (modVal != '') {
                v = modVal;
            }
            value = v2;
        } else {
            value = moduleData[field];
        }
        return value;
    };
    $scope.getModuleType = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.name;
        return ret;
    };
    $scope.tags = [];
    $scope.getModuleUI = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.name;
        return ret;
    };
    $scope.getModuleLabel = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = i18n.t(found.label);

        return ret;
    };
    $scope.getBlockLabel = function(label) {
        var ret = i18n.t(label);
        return ret;
    };
    $scope.getPickListVal = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.picklistValues;
        return ret;
    };
    $scope.onSelectTypeAhead = function($item, $model, $label, c) {
        $scope.moduleData[c] = $item.id;
        var temp_ui10 = c + '_tempui10';
        $scope[temp_ui10] = $label;
    };
    $scope.descForUi10 = function(module, curr_ui10) {
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module).then(function(response2) {
                var aarray = [],
                    a_ex = [];
                if (response.data.result.labelFields.indexOf(',') != -1) {
                    a_ex = response.data.result.labelFields.split(',');
                } else {
                    a_ex[0] = response.data.result.labelFields;
                }
                angular.forEach(response2.data.result, function(value, key) {
                    var name = '';
                    for (var k = 0; k < a_ex.length; k++) {
                        name += ' ' + value[a_ex[k]];
                    }
                    aarray.push({
                        val: name,
                        id: value.id
                    });
                });
                var fld_autoc = curr_ui10 + '_autocom';
                $scope[fld_autoc] = aarray;
            });
        });
    };
    $scope.exists = function(item, list) {
        return list.indexOf(item) > -1;
    };
    $scope.toggle = function(item, list, field) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);

        $scope.fixMultiSelectValues(field);
        //$scope.doUpdateAccount();
    };
    $scope.fixMultiSelectValues = function(field) {
        if ($scope.chipSelectedOptions[field].length > 1) {
            $scope.moduleData[field] = $scope.chipSelectedOptions[field].join(" |##| ");
        }
    }
    $scope.autocompleteUiRefers = function(modulefields) {
        var ui10Arr = $filter('getArrayType10')(modulefields);
        $scope.ui10Arr = ui10Arr;
        $scope.i = 0;
        var len = $scope.ui10Arr.length;
        var ui10_fields = new Array();
        for (; $scope.i < len; $scope.i++) {
            if ($scope.ui10Arr[$scope.i]['type']['refersTo'] != '') {
                var mod = $scope.ui10Arr[$scope.i]['name'] + '_autocommod';
                $scope[mod] = $scope.ui10Arr[$scope.i]['type']['refersTo'];
                $scope.descForUi10($scope.ui10Arr[$scope.i]['type']['refersTo'], $scope.ui10Arr[$scope.i]['name']);
            }
        }
    };
    $scope.getModuleRefers = function(columns) {
        var mod = columns + '_autocommod';
        return $scope[mod][0];
    };
    $scope.getIdRefers = function(columns) {
        var mod = columns + '_autocomid';
        return $scope[mod];
    };

    $scope.first_modific = true;
    $scope.id_just_created = '';
    //an array of files selected
    $scope.files = [];
    //listen for the file selected event
    $scope.$on("fileSelected", function (event, args) {
        $scope.$apply(function () {            
            //add the file object to the scope's files collection
            $scope.files.push(args.file);
            $scope.moduleData['filename']='file';
            console.log($scope.files);
        });
    });
    coreBOSWSAPI.doDescribe($scope.module).then(function(response) {
        $scope.idPrefix = response.data.result.idPrefix;
        $scope.createable = response.data.result.createable;
        $scope.updateable = response.data.result.updateable;
        $scope.deleteable = response.data.result.deleteable;
        $scope.retrieveable = response.data.result.retrieveable;
        $scope.modulename = response.data.result.name;
        $scope.modulefields = response.data.result.fields;
        $scope.labelFields = response.data.result.labelFields;
        $scope.indexFields = response.data.result.indexFields;
        $scope.portalBlock = false;
        $scope.dealer = coreBOSAPIStatus.getSessionInfo()._patientid;
        $scope.contactid = coreBOSAPIStatus.getSessionInfo()._contactid;
        $scope.grpid = coreBOSAPIStatus.getSessionInfo()._grpid;
        $scope.userid = coreBOSAPIStatus.getSessionInfo()._userid;

        if ($scope.createable) {
            $scope.saveModule = function(label, value, type) {
                var hasFileField=false;
                if ($scope.mandatoryLogic()) {
                    angular.forEach($scope.moduleData, function(value, key) {
                        var type=$scope.getModuleType(key,$scope.modulefields);
                        if (type == 'date') {
                            $scope.moduleData[key] = moment(value).format('YYYY-MM-DD');;
                        }
                        else if(type == 'file'){
                            hasFileField=true;
                            if($scope.files.length>0){
                              $scope.moduleData[key]=$scope.files[0].name;
                              $scope.moduleData['filelocationtype']='I';
                              $scope.moduleData['filestatus']='1';
                           }
                        }
                    });  
                    if($scope.moduleData['assigned_user_id'] == null) {
				$scope.moduleData['assigned_user_id'] = coreBOSAPIStatus.getSessionInfo()._userid;
			}
                    if(hasFileField){
                        $http({
                            method: 'POST',
                            url: Setup.corebosapi+'/upload.php?record=&module='+$routeParams.module+'&models='+encodeURIComponent(JSON.stringify($scope.moduleData)),
                            headers: { 'Content-Type': undefined },
                            transformRequest: function (data) {
                                var formData = new FormData();
                                for (var i = 0; i < data.files.length; i++) {
                                    formData.append("filename" , data.files[i]);
                                }
                                return formData;
                            },
                            data: { files: $scope.files}
                        })
                        .then(function successCallback(response) {
                            $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                          }, function errorCallback(response) {
                            $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                         });
                    }
                    else{
                        coreBOSWSAPI.doCreate($routeParams.module, $scope.moduleData,$scope.files).then(function(response) {
                            $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                        });

                    }

                }

            };
            $scope.doCreate = function() {
                coreBOSWSAPI.doCreate('moduleData', $scope.moduleData).then(function(response) {});
            };
            $scope.doDelete = function() {
                coreBOSWSAPI.doDelete($scope.moduleData.id).then(function(response) {});
            };
            $scope.submitForm = function(form) {
                $scope.saveModule();
            };
            $scope.creating = true;
            coreBOSWSAPI.doRetrievePortalBlocks($routeParams.module).then(function(response) {
                var flds = [],
                    cols = [],
                    blocks = [];
                var refData = {};
                var chipOptions = {};
                var chipSelectedOptions = {};
                var moduleData = {};
                if (response.data.result[0] != '' && response.data.result[0] != undefined) {
                    $scope.portalBlock = true;
                    angular.forEach(response.data.result.info, function(value, key) {
                        if (key == $scope.labelFields) {
                            $scope.accountname = value;
                        }
                    });
                    angular.forEach(response.data.result, function(value, key) {
                        if (key != 'info') {
                            for (var k = 0; k < value.mapstructure.length; k++) {
                                var resp_arr = value.mapstructure[k];
                                var name = value.blocks[k];
                                var block = {
                                    blockname: name,
                                    rows: resp_arr
                                };
                                blocks[k] = block;
                                for (var i = 0; i < block.rows.length; i++) {
                                    for (var j = 0; j < block.rows[i].length; j++) {
                                        var thisID = block.rows[i][j];
                                        var found = $filter('getArrayElementById')($scope.modulefields, thisID, 'name');

                                        //moduleData[key] = '';
                                        //moduleData['assigned_user_id'] = '19x1';
                                        if (found != null) {
                                            $scope.moduleEditable[thisID] = found.editable;
                                            $scope.moduleMandatory[thisID] = found.mandatory;
                                            if (found.type.name == 'reference') {
                                                refData[thisID] = found.type.refersTo + "";
                                                $scope.getUi10Readable(found.type.refersTo, found.value, thisID);
                                                $scope.getUi10List(thisID,found.type.refersTo);
                                            }
                                            if (found.type.name == 'evomultireference') {
                                                $scope.getUiEvoReadable( found.value, thisID);
                                                $scope.getUiEvoList(thisID);
                                            }
                                            if (found.type.name == 'boolean') { // <<---FIX
                                                moduleData[key] = ((moduleData[key] == '0' || moduleData[key] == null || moduleData[key] == '') ? false : true);
                                            }
                                            if (found.type.name == 'picklist') {
                                                var pl = {
                                                    defaultValue: found.type.defaultValue,
                                                    picklistValues: found.type.picklistValues
                                                };
                                            }
                                            if (found.type.name == 'multipicklist') {
                                                var tempArr = found.type.picklistValues;
                                                var tempVals = [];
                                                for (var i in tempArr) {
                                                    tempVals.push(tempArr[i].value);
                                                }
                                                chipOptions[key] = tempVals;
                                            }
                                        }
                                    }
                                }
                            };
                        }
                        console.log(blocks);
                    });
                    $scope.blocks = blocks;
                    flds.push(cols);
                    $scope.modulefieldList = flds;
                    $scope.moduleData = moduleData;
                    $scope.type10referals = refData;
                    $scope.chipOptions = chipOptions;
                    $scope.chipSelectedOptions = chipSelectedOptions;
                    // console.log("$scope.moduleData4", $scope.moduleData);
                } else {
                    coreBOSWSAPI.doRetrieve($routeParams.id).then(function(response) {
                        var flds = [],
                            cols = [];
                        var moduleData = {};
                        var numcols = 3;
                        var lblclass = 'col-md-2',
                            vlclass = 'col-md-2';
                        angular.forEach(response.data.result, function(value, key) {
                            var found = $filter('getArrayElementById')($scope.modulefields, key, 'name');
                            if (key == $scope.labelFields) {
                                $scope.accountname = value;
                            }
                            var fld = {
                                label: found.label,
                                labelclass: lblclass,
                                field: key,
                                val: value == '' ? 'none' : value,
                                valclass: vlclass,
                                uitype: found.uitype,
                                type: found.type.name
                            };
                            helpdesk[key] = value;

                            $scope.moduleEditable[fld.field] = found.editable;
                            $scope.moduleMandatory[fld.field] = found.mandatory;

                            if (found.type.name == 'reference') {
                                refData[fld.field] = found.type.refersTo + "";
                                $scope.getUi10Readable(found.type.refersTo, value, key);

                                $scope.getUi10List(found.type.refersTo);
                            }
                            if (found.type.name == 'picklist') {
                                var pl = {
                                    defaultValue: found.type.defaultValue,
                                    picklistValues: found.type.picklistValues
                                };
                                angular.extend(flds, pl);
                                if (flds.field == 'ticketstatus') {
                                    $scope.ticketstatus = flds;
                                }
                            }
                            cols.push(fld);
                            if (cols.length == numcols) {
                                flds.push(cols);
                                cols = [];
                            }
                        });
                        flds.push(cols);
                        $scope.modulefieldList = flds;
                        $scope.moduleData = moduleData;
                        // console.log("$scope.moduleData1", $scope.moduleData);
                    });
                }
            });
        }
    });

    $scope.ulogged = coreBOSAPIStatus.getSessionInfo()._userid;
    $scope.uid = $scope.ulogged.split("x");
    
})

.controller('CreateViewCtrlRelation', function($scope, $i18next, $http, $routeParams, FileUploader, $filter, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.modulefieldList = [{
        field: '',
        val: ''
    }];
    $scope.module = $routeParams.module;
    $scope.relmodule = $routeParams.relmodule;
    $scope.id = $routeParams.id;
    $scope.relatedfield = $routeParams.relatedfield;
    $scope.iscreatedview = false;
    $scope.moduleData = {};
    $scope.entity = {};
    $scope.creating = false;
    $scope.type10referals = {};
    $scope.ui10Readable = {};
    $scope.ui10Readable = {};
    $scope.usersList = {};
    $scope.groupList = {};
    $scope.type10list = {};
    $scope.hidden = true;

    $scope.chipSelectedItem = null;
    $scope.chipOptions = {};
    $scope.chipSelectedOptions = {}
    $scope.chipNumberChips = [];
    $scope.chipNumberChips2 = [];
    $scope.chipNumberBuffer = '';

    $scope.moduleEditable = {};
    $scope.moduleMandatory = {};
    $scope.mandatory_fields = {};

    $scope.formatedDates = {};

    $scope.getUsers = function() {
        coreBOSWSAPI.doQuery("SELECT id, first_name,last_name FROM Users").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.last_name + " " + value.first_name
                });
            });
            $scope.usersList = item;
        });
    }
    $scope.getGroups = function() {
        coreBOSWSAPI.doQuery("SELECT id, groupname FROM Groups").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.groupname
                });
            });
            $scope.groupList = item;
        });
    }



    $scope.autofill = function(idvin) {
        coreBOSWSAPI.getAutocompletefields(idvin).then(function(response) {

                var tempItem = response.data.result[0];
                $scope.thisRef = tempItem.productname;
                $scope.ui10Readable['productcase'] = tempItem.productname;
                $scope.moduleData['productcase'] = "14x" + tempItem.productvin;
                $scope.moduleData['cf_998'] = tempItem.cf_982;
                $scope.ui10Readable['cf_998'] = tempItem.productname;

                // console.log("RESPONSE", tempItem);
                // $scope.cf_998 = response.data.result[0].productvin;
                // $scope.productcase = response.data.result[0].productvin;
                // $scope.productcase_display = response.data.result[0].productname;
            }, function(response) {
                console.log("RESPONSE", response);
            }

        );
    }

    $scope.autofillVIN = function(idvin) {

        coreBOSWSAPI.getAutocompletefieldsVIN(idvin).then(function(response) {
                var tempItem = response.data.result[0];
                $scope.thisRef = tempItem.productname;
                $scope.moduleData['cf_998'] = tempItem.cf_982;
                $scope.ui10Readable['cf_998'] = tempItem.productname;
            }, function(response) {}

        );
    }


    $scope.ownerUpdated = function(item, model) {
        $scope.thisOwner = item.name;
        $scope.moduleData["assigned_user_id"] = item.id;
        $scope.debounceSaveUpdates("assigned_user_id", item.id);
    }
    $scope.refUpdated = function(item, model, label) {
        $scope.thisRef = item.name;
        $scope.ui10Readable[label] = item.name;
        $scope.moduleData[label] = item.id;
        // console.log("RESPONSE3", item, model, label);
        if (label == 'vinproducts') {
            $scope.autofill(item.id);
            $scope.getproductfamily(item.id);
        }
        if (label == 'productcase') {
            $scope.autofillVIN(item.id);
        }

        //$scope.debounceSaveUpdates(label, item.id);
    }
    $scope.getUsers();
    $scope.getGroups();

    $scope.adjustDateS = function(label, value) {
        $scope.moduleData[label] = moment(value).format('YYYY-MM-DD');
    };

    $scope.doCancel = function() {
        $location.url("/portal_module/" + $scope.module + "/" + $scope.module);
    };

    coreBOSWSAPI.getFieldDep($routeParams.module, 'FieldDependencyPortal').then(function(response) {
        $scope.map_field_dep = response.data.result.all_field_dep;
        $scope.MAP_RESPONSIBILE_FIELDS = response.data.result.MAP_RESPONSIBILE_FIELDS;
        $scope.MAP_RESPONSIBILE_FIELDS3 = response.data.result.MAP_RESPONSIBILE_FIELDS3;
        $scope.MAP_RESPONSIBILE_FIELDS2 = response.data.result.MAP_RESPONSIBILE_FIELDS2;
        $scope.MAP_PCKLIST_TARGET = response.data.result.MAP_PCKLIST_TARGET;

    });
    $scope.showLogic = function(field) {
        var ret = true;
        var fieldname = '';
        angular.forEach($scope.map_field_dep, function(value, key) {
            var conditionResp = '';
            if (value.targetfield.indexOf(field) != -1) {
                angular.forEach(value.respfield, function(resp_val, resp_val_key) {
                    var resp_value = value.respvalue_portal[resp_val_key];
                    var comparison = value.comparison[resp_val_key];
                    if (resp_val_key !== 0) {
                        if (comparison === 'empty' || comparison === 'notempty')
                            conditionResp += ' || ';
                        else
                            conditionResp += ' && ';
                    }
                    if (comparison === 'empty')
                        conditionResp += $scope.moduleData[resp_val] === '' || $scope.moduleData[resp_val] === undefined;
                    else if (comparison === 'notempty')
                        conditionResp += $scope.moduleData[resp_val] !== '' && $scope.moduleData[resp_val] !== undefined;
                    else {
                        conditionResp += resp_value.indexOf($scope.moduleData[resp_val]) != -1;
                    }
                });

                angular.forEach(value.targetfield, function(target_fld, target_fld_key) {
                    if (field == target_fld && value.action[target_fld_key] == 'hide' && eval(conditionResp)) {
                        ret = false;
                        fieldname = field;
                    } else if (field == target_fld && value.action[target_fld_key] == 'show' && eval(conditionResp)) {
                        ret = true;
                        fieldname = field;
                    } else if (field == target_fld && fieldname != field && value.action[target_fld_key] == 'hide') {
                        ret = false;
                        fieldname = field;
                    }
                });
            }
        });
        return ret;
    };

    $scope.mandatoryLogic = function() {
        var ret = true;
        var fieldname = '';
        var blocks = [];
        angular.forEach($scope.map_field_dep, function(value, key) {
            var keepGoing = true;
            angular.forEach(value.targetfield, function(target_fld, target_fld_key) {
                if (keepGoing) {
                    var conditionResp = '';
                    angular.forEach(value.respfield, function(resp_val, resp_val_key) {
                        var resp_value = value.respvalue_portal[resp_val_key];
                        var comparison = value.comparison[resp_val_key];
                        if (resp_val_key !== 0) {
                            if (comparison === 'empty' || comparison === 'notempty')
                                conditionResp += ' || ';
                            else
                                conditionResp += ' && ';
                        }
                        if (comparison === 'empty')
                            conditionResp += ($scope.moduleData[resp_val] === '' || $scope.moduleData[resp_val] === undefined);
                        else if (comparison === 'notempty')
                            conditionResp += ($scope.moduleData[resp_val] !== '' && $scope.moduleData[resp_val] !== undefined);
                        else {
                            conditionResp += resp_value.indexOf($scope.moduleData[resp_val]) != -1;
                        }
                    });
                    // console.log(conditionResp + ' ' + $scope.moduleData[target_fld])
                    if (value.mandatory[target_fld_key] == 'mandatory' && eval(conditionResp) && $scope.moduleData[target_fld] == undefined) {
                        ret = false;
                        var mand = target_fld + '_mandText';
                        $scope[mand] = true;
                        var field_data = {
                            name: target_fld
                        };
                        blocks.push(field_data);
                    }
                }
            });
        });
        $scope.mandatory_fields = blocks;
        return ret;
    };

    $scope.getMandatoryText = function(fld) {
        var mand = fld + '_mandText';
        var text = '';
        if ($scope[mand]) {
            text = $scope.getModuleLabel(fld, $scope.modulefields) + ' is mandatory';
        }
        for (var i = $scope.mandatory_fields.length - 1; i >= 0; i--) {
            var fld_mand = $scope.mandatory_fields[i].name;
            var temp_mand = fld_mand + '_mandText';
            if ($scope.moduleData[fld_mand] !== undefined) {
                $scope.mandatory_fields.splice(i, 1);
                $scope[temp_mand] = false;
            }
        }
        return text;
    };

    $scope.getUi10Readable = function(module, curr_ui10, label) {
        $scope.ui10Readable[label] = "Empty";
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module + ' where id=' + curr_ui10).then(function(response2) {
                var value;
                if (module != "Contacts") {
                    value = response2.data.result[Object.keys(response2.data.result)[0]][response.data.result.labelFields];
                } else {
                    value = response2.data.result[Object.keys(response2.data.result)[0]];
                    value = value.firstname + " " + value.lastname;
                }
                $scope.ui10Readable[label] = value;
            });
        });
    };
    $scope.getUi10List = function(moduleName) {
        coreBOSWSAPI.doDescribe(moduleName).then(function(response) {
            coreBOSWSAPI.doQuery('SELECT ' + response.data.result.labelFields + ' FROM ' + moduleName).then(function(response2) {
                var item = []
                var thisModuleData = [];
                angular.forEach(response2.data.result, function(value, key) {
                    angular.forEach(value, function(value2, key2) {
                        if (moduleName != "Contacts") {
                            if (key2 == 'id') {
                                item.id = value2;
                            } else {
                                item.name = value2;
                            }
                        }
                    });
                    if (moduleName == "Contacts") {
                        item.id = value.id;
                        item.name = value.firstname + " " + value.lastname;
                    }
                    thisModuleData.push({
                        name: item.name,
                        id: item.id,
                        family: item.famliy
                    });
                });
                $scope.type10list[moduleName] = thisModuleData;
            });
        });
    }
    $scope.getModuleValue = function(field, moduleData, modulefields) {
        var type = '',
            value;
        type = $scope.getModuleType(field, modulefields);
        if (type == 'boolean') {
            value = ((moduleData[field] == '0' || moduleData[field] == null || moduleData[field] == '') ? 'No' : 'Si');
        } else if (type == 'reference') {
            if (moduleData[field] != undefined) {
                var temp_ui10 = field + '_tempui10';
                value = moduleData[field].value10;
                var temp_id = field + '_autocomid';
                $scope[temp_id] = moduleData[field].id10;
                if (value == '' || value == null) {
                    value = $scope[temp_ui10];
                }
                $scope[temp_ui10] = value;
            } else {
                value = '';
            }
        } else if (type == 'date' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy');
        } else if (type == 'datetime' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy HH:mm:ss');
        } else if (type == 'multipicklist') {
            var modVal = moduleData[field];
            var v2 = modVal;
            var v = '';
            if (modVal.indexOf('|##|') != -1) {
                v = modVal.split('|##|');
                v2 = v.join(',');
            } else if (angular.isArray(modVal)) {
                v = modVal;
                v2 = modVal.join(',');
            } else if (modVal != '') {
                v = modVal;
            }
            value = v2;
        } else {
            value = moduleData[field];
        }
        return value;
    };
    $scope.getModuleType = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.name;
        return ret;
    };
    $scope.getModuleLabel = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.label;

        return ret;

    };
    $scope.getPickListVal = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.picklistValues;
        return ret;
    };
    $scope.onSelectTypeAhead = function($item, $model, $label, c) {
        $scope.moduleData[c] = $item.id;
        var temp_ui10 = c + '_tempui10';
        $scope[temp_ui10] = $label;
    };
    $scope.descForUi10 = function(module, curr_ui10) {
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module).then(function(response2) {
                var aarray = [],
                    a_ex = [];
                if (response.data.result.labelFields.indexOf(',') != -1) {
                    a_ex = response.data.result.labelFields.split(',');
                } else {
                    a_ex[0] = response.data.result.labelFields;
                }
                angular.forEach(response2.data.result, function(value, key) {
                    var name = '';
                    for (var k = 0; k < a_ex.length; k++) {
                        name += ' ' + value[a_ex[k]];
                    }
                    aarray.push({
                        val: name,
                        id: value.id
                    });
                });
                var fld_autoc = curr_ui10 + '_autocom';
                $scope[fld_autoc] = aarray;
            });
        });
    };
    $scope.exists = function(item, list) {
        return list.indexOf(item) > -1;
    };
    $scope.toggle = function(item, list, field) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);

        $scope.fixMultiSelectValues(field);
        //$scope.doUpdateAccount();
    };
    $scope.fixMultiSelectValues = function(field) {
        if ($scope.chipSelectedOptions[field].length > 1) {
            $scope.moduleData[field] = $scope.chipSelectedOptions[field].join(" |##| ");
        }
    }
    $scope.autocompleteUiRefers = function(modulefields) {
        var ui10Arr = $filter('getArrayType10')(modulefields);
        $scope.ui10Arr = ui10Arr;
        $scope.i = 0;
        var len = $scope.ui10Arr.length;
        var ui10_fields = new Array();
        for (; $scope.i < len; $scope.i++) {
            if ($scope.ui10Arr[$scope.i]['type']['refersTo'] != '') {
                var mod = $scope.ui10Arr[$scope.i]['name'] + '_autocommod';
                $scope[mod] = $scope.ui10Arr[$scope.i]['type']['refersTo'];
                $scope.descForUi10($scope.ui10Arr[$scope.i]['type']['refersTo'], $scope.ui10Arr[$scope.i]['name']);
            }
        }
    };
    $scope.getModuleRefers = function(columns) {
        var mod = columns + '_autocommod';
        return $scope[mod][0];
    };
    $scope.getIdRefers = function(columns) {
        var mod = columns + '_autocomid';
        return $scope[mod];
    };

    $scope.first_modific = true;
    $scope.id_just_created = '';
    coreBOSWSAPI.doDescribe($scope.module).then(function(response) {
        $scope.idPrefix = response.data.result.idPrefix;
        $scope.createable = response.data.result.createable;
        $scope.updateable = response.data.result.updateable;
        $scope.deleteable = response.data.result.deleteable;
        $scope.retrieveable = response.data.result.retrieveable;
        $scope.modulename = response.data.result.name;
        $scope.modulefields = response.data.result.fields;
        $scope.labelFields = response.data.result.labelFields;
        $scope.indexFields = response.data.result.indexFields;
        $scope.portalBlock = false;
        $scope.dealer = coreBOSAPIStatus.getSessionInfo()._patientid;
        $scope.contactid = coreBOSAPIStatus.getSessionInfo()._contactid;
        $scope.grpid = coreBOSAPIStatus.getSessionInfo()._grpid;
        $scope.userid = coreBOSAPIStatus.getSessionInfo()._userid;

        if ($scope.createable) {
            $scope.saveModule = function(label, value, type) {
                if ($scope.mandatoryLogic()) {
                    if ($routeParams.module == 'Cases') {
                        var extra_val = {
                            cf_1015: 'open',
                            customercase: $scope.dealer,
                            contactcase: $scope.contactid,
                            flowtype: 'Customer Tech Support',
                            cf_988: 'First Level',
                            assigned_user_id: $scope.userid
                        };
                        $scope.moduleData = angular.extend(extra_val, $scope.moduleData);
                    }


                    coreBOSWSAPI.doCreate($routeParams.module, $scope.moduleData).then(function(response) {
                        $scope.id_just_created = response.data.result['id'];
                        if ($routeParams.module == 'Cases') {
                            var resp_doc = $scope.just_created_doc.split('@@');
                            var up_doc = {
                                messagetocase: $scope.id_just_created,
                                id: '15x' + resp_doc[0],
                                notes_title: resp_doc[1]
                            };
                            coreBOSWSAPI.doUpdate('Documents', up_doc).then(function(response) {
                                // console.log("data saved");
                                $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                            });
                        }
                        if ($routeParams.module == 'Documents') {
                            if ($scope.just_created_doc != '') {
                                // alert("herehere");
                                // alert($scope.id_just_created);
                                $http.get("http://193.182.16.151/SAME/removeredundant.php?idtoremove=" + $scope.id_just_created.split('x')[1]).success(function(data) {});
                                $location.path('/portal_moduleview/' + 'Cases' + '/' + $scope.moduleData.messagetocase);
                            } else {
                                var resp_doc = $scope.just_created_doc.split('@@');
                                // console.log('tjeter' + resp_doc + JSON.stringify($scope.moduleData.messagetocase));
                                $http.get('http://193.182.16.151/SAME/uploadrelated.php?param1=' + $scope.moduleData.messagetocase.split("x")[1] + '&param2=' + resp_doc[0]).success(function(data) {
                                    $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                                });
                            }
                        }
                        $location.path('/portal_module/' + $routeParams.module + '/' + $routeParams.module);
                    });

                }

            };
            $scope.doCreate = function() {
                coreBOSWSAPI.doCreate('moduleData', $scope.moduleData).then(function(response) {});
            };
            $scope.doDelete = function() {
                coreBOSWSAPI.doDelete($scope.moduleData.id).then(function(response) {});
            };
            $scope.submitForm = function(form) {
                $scope.saveModule();
            };
            $scope.creating = true;
            coreBOSWSAPI.doRetrievePortalBlocks($routeParams.module).then(function(response) {
                var flds = [],
                    cols = [],
                    blocks = [];
                var refData = {};
                var chipOptions = {};
                var chipSelectedOptions = {};
                $scope.moduleData = {};
                var moduleData = {};
                if (response.data.result[0] != '' && response.data.result[0] != undefined) {
                    $scope.portalBlock = true;
                    angular.forEach(response.data.result.info, function(value, key) {
                        if (key == $scope.labelFields) {
                            $scope.accountname = value;
                        }
                    });
                    angular.forEach(response.data.result, function(value, key) {
                        if (key != 'info') {
                            angular.forEach(value.mapstructure, function(val, ke) {
                                var name = ke;
                                var block = {
                                    blockname: name,
                                    rows: val
                                };
                                blocks.push(block);
                                for (var i = 0; i < block.rows.length; i++) {
                                    for (var j = 0; j < block.rows[i].length; j++) {
                                        var thisID = block.rows[i][j];
                                        var found = $filter('getArrayElementById')($scope.modulefields, thisID, 'name');

                                        //moduleData[key] = '';
                                        //moduleData['assigned_user_id'] = '19x1';
                                        if (found != null) {
                                            $scope.moduleEditable[thisID] = found.editable;
                                            $scope.moduleMandatory[thisID] = found.mandatory;
                                            if (found.type.name == 'reference') {
                                                if (thisID == $scope.relatedfield) {
                                                    found.value = $scope.id;
                                                    moduleData[thisID] = found.value;
                                                }
                                                refData[thisID] = found.type.refersTo + "";
                                                $scope.getUi10Readable(found.type.refersTo, found.value, thisID);
                                                $scope.getUi10List(found.type.refersTo);
                                                //console.log("must send identifier", found.type.refersTo, value, key);
                                                //$scope.getUi10Readable(found.type.refersTo, fld.val, key);
                                            }
                                            if (found.type.name == 'boolean') { // <<---FIX
                                                moduleData[key] = ((moduleData[key] == '0' || moduleData[key] == null || moduleData[key] == '') ? false : true);
                                            }
                                            if (found.type.name == 'picklist') {
                                                var pl = {
                                                    defaultValue: found.type.defaultValue,
                                                    picklistValues: found.type.picklistValues
                                                };
                                            }
                                            if (found.type.name == 'multipicklist') {
                                                var tempArr = found.type.picklistValues;
                                                var tempVals = [];
                                                for (var i in tempArr) {
                                                    tempVals.push(tempArr[i].value);
                                                }
                                                chipOptions[key] = tempVals;
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    });
                    $scope.blocks = blocks;
                    flds.push(cols);
                    $scope.modulefieldList = flds;
                    $scope.moduleData = moduleData;
                    $scope.type10referals = refData;
                    $scope.chipOptions = chipOptions;
                    $scope.chipSelectedOptions = chipSelectedOptions;
                    // console.log("$scope.moduleData4", $scope.moduleData);
                } else {
                    coreBOSWSAPI.doRetrieve($routeParams.id).then(function(response) {
                        var flds = [],
                            cols = [];
                        var moduleData = {};
                        var numcols = 3;
                        var lblclass = 'col-md-2',
                            vlclass = 'col-md-2';
                        angular.forEach(response.data.result, function(value, key) {
                            var found = $filter('getArrayElementById')($scope.modulefields, key, 'name');
                            if (key == $scope.labelFields) {
                                $scope.accountname = value;
                            }
                            var fld = {
                                label: found.label,
                                labelclass: lblclass,
                                field: key,
                                val: value == '' ? 'none' : value,
                                valclass: vlclass,
                                uitype: found.uitype,
                                type: found.type.name
                            };
                            helpdesk[key] = value;

                            $scope.moduleEditable[fld.field] = found.editable;
                            $scope.moduleMandatory[fld.field] = found.mandatory;

                            if (found.type.name == 'reference') {
                                refData[fld.field] = found.type.refersTo + "";
                                $scope.getUi10Readable(found.type.refersTo, value, key);

                                $scope.getUi10List(found.type.refersTo);
                            }
                            if (found.type.name == 'picklist') {
                                var pl = {
                                    defaultValue: found.type.defaultValue,
                                    picklistValues: found.type.picklistValues
                                };
                                angular.extend(flds, pl);
                                if (flds.field == 'ticketstatus') {
                                    $scope.ticketstatus = flds;
                                }
                            }
                            cols.push(fld);
                            if (cols.length == numcols) {
                                flds.push(cols);
                                cols = [];
                            }
                        });
                        flds.push(cols);
                        $scope.modulefieldList = flds;
                        $scope.moduleData = moduleData;
                        // console.log("$scope.moduleData1", $scope.moduleData);
                    });
                }
            });
        }
    });
})

.controller('moduleViewCtrl', function($scope, $http, ngTableParams, $i18next, $window, $routeParams, $filter, $location, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.modulefieldList = [{
        field: '',
        val: ''
    }];
    $scope.module = $routeParams.module;
    $scope.module1 = $routeParams.module.toLowerCase();
    $scope.recordid = $routeParams.id;
    $scope.moduleData = {};
    $scope.entity = {};
    $scope.creating = false;
    $scope.type10referals = {};
    $scope.ui10Readable = {};
    $scope.uiEvoReadable={};
    $scope.usersList = {};
    $scope.groupList = {};
    $scope.type10list = {};
    $scope.hidden = true;

    $scope.chipSelectedItem = null;
    $scope.chipOptions = {};
    $scope.chipSelectedOptions = {}
    $scope.chipNumberChips = [];
    $scope.chipNumberChips2 = [];
    $scope.chipNumberBuffer = '';

    $scope.moduleEditable = {};
    $scope.moduleMandatory = {};

    $scope.formatedDates = {};
    $scope.accountname = '';
    $scope.headername = '';


    $scope.runAction = function(actionid, val, selectedids) {
        var arr_sel = selectedids;

        var senddata = {
            'recordid': arr_sel,
            'actionid': actionid,
            'next_sub': val,
            'userid': coreBOSAPIStatus.getSessionInfo()._userid
        };
        var values = senddata;
        coreBOSWSAPI.doRunAction(values).then(function(response) {
            $scope.returnedResults = response.data.result;
        });
        if (val == 'closed') {
            alert('The ticket is closed succesfully');
            $scope.moduleData['cf_1015'] = 'closed';
        } else {
            alert('The ticket is not accepted');
            $scope.moduleData['cf_1015'] = 'assigned';
        }
    };
    $scope.getUsers = function() {
        coreBOSWSAPI.doQuery("SELECT id, first_name,last_name FROM Users").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.last_name + " " + value.first_name
                });
            });
            $scope.usersList = item;
        });
    }
    $scope.getGroups = function() {
        coreBOSWSAPI.doQuery("SELECT id, groupname FROM Groups").then(function(response) {
            var item = [];
            angular.forEach(response.data.result, function(value, key) {
                item.push({
                    id: value.id,
                    name: value.groupname
                });
            });
            $scope.groupList = item;
        });
    }
    $scope.ownerUpdated = function(item, model) {
        $scope.thisOwner = item.name;
        $scope.moduleData["assigned_user_id"] = item.id;
        $scope.debounceSaveUpdates("assigned_user_id", item.id);
    }
    $scope.refUpdated = function(item, model, label) {
        $scope.thisRef = item.name;
        $scope.ui10Readable[label] = item.name;
        $scope.moduleData[label] = item.id;
        $scope.debounceSaveUpdates(label, item.id);
    }
    $scope.getUsers();
    $scope.getGroups();

    $scope.debounceSaveUpdates = function(label, value, type) {
        console.log("Saving: ", label, value, $scope.moduleData);
        if (type == 'boolean') {
            $scope.adjustBooleanS(label, value)
        };
        if (type == 'date') {
            $scope.adjustDateS(label, value)
        };

        $scope.doUpdateAccount();

        if (type == 'boolean') {
            $scope.adjustBooleanR(label, value)
        };
    };
    $scope.doUpdateAccount = function() {
        coreBOSWSAPI.doUpdate($scope.module, $scope.moduleData).then(function(response) {
            console.log("data saved");
        }, function(response) {
            console.log("data error", $scope.module, $scope.moduleData, response)
        });
    };
    $scope.adjustBooleanS = function(label, value) {
        $scope.moduleData[label] = ((value == false) ? '0' : '1');
    };
    $scope.adjustBooleanR = function(label, value) {
        $scope.moduleData[label] = (($scope.moduleData[label] == '0') ? false : true);
    };
    $scope.adjustDateS = function(label, value) {
        $scope.moduleData[label] = moment(value).format('YYYY-MM-DD');
    };
    $scope.getUi10Readable = function(module, curr_ui10, label) {
        $scope.ui10Readable[label] = "Empty";
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module + ' where id=' + curr_ui10).then(function(response2) {
                var value;
                if(response2.data.result[0]!=undefined){
                    if (module != "Contacts") {
                        value = response2.data.result[Object.keys(response2.data.result)[0]][response.data.result.labelFields];
                    } else {
                        value = response2.data.result[Object.keys(response2.data.result)[0]];
                        value = value.firstname + " " + value.lastname;
                    }
                    $scope.ui10Readable[label] = value;
                }
            });
        });
    };
    
    $scope.getUiEvoReadable = function( curr_ui10, label) {
        $scope.uiEvoReadable[label] = [{'crmname':"Empty"}];
        coreBOSWSAPI.getEvoActualAutocomplete(curr_ui10,label).then(function(response2) {
            $scope.uiEvoReadable[label]=response2.data.result;
        });
        
    };

    $scope.getUi10List = function(moduleName) {
        coreBOSWSAPI.doDescribe(moduleName).then(function(response) {
            coreBOSWSAPI.doQuery('SELECT ' + response.data.result.labelFields + ' FROM ' + moduleName).then(function(response2) {
                var item = []
                var thisModuleData = [];
                angular.forEach(response2.data.result, function(value, key) {
                    angular.forEach(value, function(value2, key2) {
                        if (moduleName != "Contacts") {
                            if (key2 == 'id') {
                                item.id = value2;
                            } else {
                                item.name = value2;
                            }
                        }
                    });
                    if (moduleName == "Contacts") {
                        item.id = value.id;
                        item.name = value.firstname + " " + value.lastname;
                    }
                    thisModuleData.push({
                        name: item.name,
                        id: item.id
                    });
                });
                $scope.type10list[moduleName] = thisModuleData;
            });
        });
    }

    $scope.getModuleValue = function(field, moduleData, modulefields) {
        var type = '',
            value;
        type = $scope.getModuleType(field, modulefields);
        if (type == 'boolean') {
            value = ((moduleData[field] == '0' || moduleData[field] == null || moduleData[field] == '') ? 'No' : 'Si');
        } else if (type == 'reference') {
            if (moduleData[field] != undefined) {
                var temp_ui10 = field + '_tempui10';
                value = moduleData[field].value10;
                var temp_id = field + '_autocomid';
                $scope[temp_id] = moduleData[field].id10;
                if (value == '' || value == null) {
                    value = $scope[temp_ui10];
                }
                $scope[temp_ui10] = value;
            } else {
                value = '';
            }
        } else if (type == 'date' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy');
        } else if (type == 'datetime' && moduleData[field] != '') {
            value = $filter('date')(new Date(moduleData[field]), 'dd-MM-yyyy HH:mm:ss');
        } else if (type == 'multipicklist') {
            var modVal = moduleData[field];
            var v2 = modVal;
            var v = '';
            if (modVal.indexOf('|##|') != -1) {
                v = modVal.split('|##|');
                v2 = v.join(',');
            } else if (angular.isArray(modVal)) {
                v = modVal;
                v2 = modVal.join(',');
            } else if (modVal != '') {
                v = modVal;
            }
            value = v2;
        } else {
            value = moduleData[field];
        }
        return value;
    };
    $scope.getModuleType = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.name;
        return ret;
    };
    $scope.downloadfile = function(path) {
        $window.open(path, '_blank');
    };
    $scope.getModuleLabel = function(field, modulefields) {
        // $scope.useriloguar=coreBOSAPIStatus.getSessionInfo()._userid;
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = i18n.t(found.label);

        return ret;
    };
    $scope.getBlockLabel = function(label) {
        var ret = i18n.t(label);
        return ret;
    };
    $scope.getPickListVal = function(field, modulefields) {
        var ret = '';
        var found = $filter('getArrayElementById')(modulefields, field, 'name');
        if (found != '' && found != undefined && found != null)
            ret = found.type.picklistValues;
        return ret;
    };
    $scope.onSelectTypeAhead = function($item, $model, $label, c) {
        $scope.moduleData[c] = $item.id;
        var temp_ui10 = c + '_tempui10';
        $scope[temp_ui10] = $label;
    };

    $scope.descForUi10 = function(module, curr_ui10) {
        coreBOSWSAPI.doDescribe(module).then(function(response) {
            coreBOSWSAPI.doQuery('select ' + response.data.result.labelFields + ' from ' + module).then(function(response2) {

                var aarray = [],
                    a_ex = [];
                if (response.data.result.labelFields.indexOf(',') != -1) {
                    a_ex = response.data.result.labelFields.split(',');
                } else {
                    a_ex[0] = response.data.result.labelFields;
                }
                angular.forEach(response2.data.result, function(value, key) {
                    var name = '';
                    for (var k = 0; k < a_ex.length; k++) {
                        name += ' ' + value[a_ex[k]];
                    }
                    aarray.push({
                        val: name,
                        id: value.id
                    });
                });
                var fld_autoc = curr_ui10 + '_autocom';
                $scope[fld_autoc] = aarray;
            });
        });
    };

    $scope.exists = function(item, list) {
        return list.indexOf(item) > -1;
    };

    $scope.toggle = function(item, list, field) {
        var idx = list.indexOf(item);
        if (idx > -1) list.splice(idx, 1);
        else list.push(item);

        $scope.fixMultiSelectValues(field);
        $scope.doUpdateAccount();
    };

    $scope.fixMultiSelectValues = function(field) {
        $scope.moduleData[field] = $scope.chipSelectedOptions[field].join(" |##| ");
    };

    $scope.querySearch = function(query) {
        $scope.curOptions = $scope.chipOptions["cf_1015"];
        var results = query ? $scope.curOptions.filter($scope.createFilterFor(query)) : [];
        return results;
    }

    $scope.createFilterFor = function(query) {
        var lowercaseQuery = angular.lowercase(query);
        return function filterFn(curOption) {
            return (curOption._lowername.indexOf(lowercaseQuery) === 0) ||
                (curOption._lowertype.indexOf(lowercaseQuery) === 0);
        };
    }

    $scope.message = {
        desc_mess: ''
    };

    $scope.createMessage = function(desc) {
        var extra_val = {
            messagesname: 'Message from ' + $scope.moduleData['casesname'],
            description: desc,
            assigned_user_id: $scope.userid
        };
        coreBOSWSAPI.doCreate('Messages', extra_val).then(function(response) {
            $scope.tableParamsMessages.reload();
            $scope.message.desc_mess = '';
            var extra_val = {
                id: $scope.recordid,
                casesname:$scope.moduleData['casesname'],
                cf_1015: 'assigned',
                cf_987: 'Assigned'
            };
//            coreBOSWSAPI.doUpdate('Cases', extra_val).then(function(response) {
//                $scope.headername = i18n.t('Assigned')+' / ' + i18n.t($scope.moduleData['cf_983']);
//                $scope.moduleData['cf_1015'] = 'assigned';
//            });
        });
    };

    $scope.tableParamsMessages = new ngTableParams({
        page: 1, // show first page
        count: 5 // count per page
    }, {
        // total: $scope.records_Messages.length, // length of data
        counts: [], // hide page counts control
        getData: function($defer, params) {
            coreBOSWSAPI.getTimeline($scope.recordid).then(function(response) {
                $scope.records_Messages = response.data.result;
                var filteredData = params.filter() ?
                    $filter('filter')($scope.records_Messages, params.filter()) :
                    $scope.records_Messages;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.records_Messages;
                params.total(orderedData.length); // set total for recalc pagination
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            });
        }
    });
    $scope.ulogged = coreBOSAPIStatus.getSessionInfo()._userid;
    $scope.uid = $scope.ulogged.split("x");
    var cid = $scope.recordid.split("x");
    

    coreBOSWSAPI.getRelatedModules($scope.module, 'PORTALSV').then(function(response) {
        if (response.data.result[0]) {
            var relmod = response.data.result[0].module;
            var module_trans = response.data.result[0].module_trans;
            coreBOSWSAPI.doGetUi10RelatedRecords($scope.recordid, $scope.module, relmod, 'PORTALSV').then(function(response) {
                $scope.graphRecords = response.data.result.records;
                $scope.optionsgraphRecords = {
                    chart: {
                        type: 'multiBarChart',
                        height: 450,
                        "width": 685,
                        margin: {
                            top: 20,
                            right: 20,
                            bottom: 60,
                            left: 65
                        },
                        clipEdge: false,
                        staggerLabels: false,
                        transitionDuration: 500,
                        stacked: false,
                        tooltips: false,
                        xAxis: {
                            axisLabel: module_trans,
                            showMaxMin: false,
                            tickFormat: function(d) {
                                return d;
                            }
                        },
                        yAxis: {
                            axisLabel: '',
                            axisLabelDistance: 20,
                            tickFormat: function(d) {
                                return d;
                            }
                        }
                    }

                };
            });
        }
    });
    $scope.formatText= function(text) {
        var value=text;
        if(value.length>30)
            value=text.substring(0, 30)+'<br/>'+text.substring(30);
            
        return value;
    }
    coreBOSWSAPI.doDescribe($scope.module).then(function(response) {
        $scope.idPrefix = response.data.result.idPrefix;
        $scope.createable = response.data.result.createable;
        $scope.updateable = response.data.result.updateable;
        $scope.deleteable = response.data.result.deleteable;
        $scope.retrieveable = response.data.result.retrieveable;
        $scope.modulename = response.data.result.name;
        $scope.modulefields = response.data.result.fields;
        $scope.labelFields = response.data.result.labelFields;
        $scope.indexFields = response.data.result.indexFields;
        $scope.portalBlock = false;
        if ($scope.retrieveable) {

            $scope.saveModule = function() {
                if ($scope.creating) {
                    coreBOSWSAPI.doCreate($routeParams.module, $scope.moduleData).then(function(response) {});
                } else {
                    coreBOSWSAPI.doUpdate($scope.module, $scope.moduleData).then(function(response) {});
                }
            };
            $scope.doCreate = function() {
                coreBOSWSAPI.doCreate('moduleData', $scope.moduleData).then(function(response) {});
            };
            $scope.doDelete = function() {
                coreBOSWSAPI.doDelete($scope.moduleData.id).then(function(response) {});
            };
            $scope.submitForm = function(form) {
                $scope.saveModule();
            };

            coreBOSWSAPI.doQuery('select first_name,last_name from users').then(function(response) {
                var uarray = [];
                angular.forEach(response.data.result, function(value, key) {
                    uarray.push(value.first_name + ' ' + value.last_name + ' (' + value.id + ')');
                });
                $scope.usrs = uarray;
            });
            coreBOSWSAPI.doRetrievePortalBlocks($routeParams.id).then(function(response) {
                    var flds = [],
                        blocks = [];
                    var moduleData = response.data.result.info;
                    var chipOptions = {};
                    var chipSelectedOptions = {};
                    var refData = {};
                    if (response.data.result[0] != '' && response.data.result[0] != undefined && response.data.result[0] != null) {
                        $scope.portalBlock = true;
                        angular.forEach(response.data.result.info, function(value, key) {
                            if (key == $scope.labelFields ) {
                                $scope.accountname = value;
                                $scope.headername = value;
                            } 
                            flds.push(key);
                        });

                        angular.forEach(response.data.result, function(value, key) {
                            if (key != 'info') {
                                //                                angular.forEach(value.mapstructure, function(val, ke) {
                                for (var m = 0; m < value.mapstructure.length; m++) {
                                    var resp_arr = value.mapstructure[m];
                                    var name = value.blocks[m];

                                    $scope.useriloguar = coreBOSAPIStatus.getSessionInfo()._userid;
                                    $scope.pickpay = coreBOSAPIStatus.getSessionInfo()._patientid;
                                    var blloknames = [];
                                    blloknames.push(name);
                                    var block = {
                                        blockname: name,
                                        rows: resp_arr
                                    };
                                    blocks[m] = block;
                                };
                            }
                        });
                        angular.forEach(response.data.result.info, function(value, key) {
                            if (key == '_downloadurl') {
                                $scope.getUrlFile = value;
                            }
                            var found1 = $filter('getArrayElementById')($scope.modulefields, key, 'name');
                            if (found1) {                                
                                if (found1 !== undefined && found1 !== null && found1 !== '') {

                                    var edit1 = false;
                                    if (key == 'description')
                                        edit1 = true;
                                    $scope.moduleEditable[key] = edit1; //found.editable;
                                    $scope.moduleMandatory[key] = found1.mandatory;
                                    if (found1.type.name == 'owner') {
                                        if (value.substring(0, 2) == "19") {
                                            coreBOSWSAPI.doQuery("SELECT last_name, first_name FROM Users WHERE id=" + value).then(function(responseUsr) {
                                                $scope.thisOwner = responseUsr.data.result[0].last_name + " " + responseUsr.data.result[0].first_name
                                            });
                                        } else {
                                            coreBOSWSAPI.doQuery("SELECT groupname FROM Groups WHERE id=" + value).then(function(responseUsr) {
                                                $scope.thisOwner = responseUsr.data.result[0].groupname
                                            });
                                        }
                                    }
                                    if (found1.type.name == 'reference') {
                                        refData[key] = found1.type.refersTo + "";
                                        $scope.getUi10Readable(found1.type.refersTo, value, key);
                                        //$scope.getUi10List(found1.type.refersTo);
                                    }
                                    if (found1.type.name == 'evomultireference') {
                                        $scope.getUiEvoReadable( value, key);
                                        //$scope.getUi10List(found1.type.refersTo);
                                    }
                                    if (found1.type.name == 'boolean') {
                                        moduleData[key] = ((moduleData[key] == '0' || moduleData[key] == null || moduleData[key] == '') ? false : true);
                                    }
                                    if (found1.type.name == 'date') {
                                        $scope.formatedDates[key] = new Date(moduleData[key]);
                                    }

                                    if (found1.type.name == 'picklist') {
                                        // var pl = {
                                        //   defaultValue: found.type.defaultValue,
                                        //   picklistValues: found.type.picklistValues
                                        // };
                                        // angular.extend(flds, pl);
                                        // if (flds.field == 'ticketstatus') {
                                        //   $scope.ticketstatus = flds;
                                        // }
                                    }
                                    if (found1.type.name == 'multipicklist') {
                                        var tempArr = $scope.getModuleValue(key, moduleData, $scope.modulefields);
                                        tempArr = tempArr.split(',');
                                        for (var i = 0; i < tempArr.length; i++) {
                                            tempArr[i] = tempArr[i].replace(/^\s*|\s*$/g, '');
                                        }
                                        chipSelectedOptions[key] = tempArr;

                                        tempArr = found1.type.picklistValues;
                                        var tempVals = [];
                                        for (var i in tempArr) {
                                            tempVals.push(tempArr[i].value);
                                        }
                                        chipOptions[key] = tempVals;
                                    }
                                }

                            }
                            //console.log("LabelsDetailView"+JSON.stringify(response.data.result));

                        });
                        $scope.blocks = blocks;
                        $scope.modulefieldList = flds;
                        $scope.moduleData = moduleData;
                        $scope.type10referals = refData;
                        $scope.chipOptions = chipOptions;
                        $scope.chipSelectedOptions = chipSelectedOptions;
                        // console.log("flds", flds);
                        // console.log("$scope.moduleData2", $scope.moduleData);
                    } else {
                        coreBOSWSAPI.doRetrieve($routeParams.id).then(function(response) {
                            var flds = [],
                                cols = [];
                            var moduleData = {};
                            var refData = {};
                            var numcols = 3;
                            var lblclass = 'col-md-2',
                                vlclass = 'col-md-2';
                            var chipOptions = {};
                            var chipSelectedOptions = {};
                            angular.forEach(response.data.result, function(value, key) {
                                var found = $filter('getArrayElementById')($scope.modulefields, key, 'name');
                                if (key == $scope.labelFields ) {
                                    $scope.accountname = value;
                                    $scope.headername = value;
                                }
                                var lbl_temp = '';
                                if (found !== undefined && found !== null && found !== '') {
                                    //console.log("Found", found, key);
                                    lbl_temp = found.label;
                                    var fld = {
                                        label: lbl_temp,
                                        labelclass: lblclass,
                                        field: key,
                                        val: value == '' ? 'none' : value,
                                        valclass: vlclass,
                                        uitype: found.uitype,
                                        type: found.type.name
                                    };
                                    moduleData[key] = value;

                                    var edit1 = false;
                                    if (key == 'description')
                                        edit1 = true;
                                    $scope.moduleEditable[key] = edit1; //found.editable;
                                    //$scope.moduleEditable[key] = found.editable;
                                    $scope.moduleMandatory[key] = found.mandatory;

                                    if (found.type.name == 'reference') {
                                        refData[fld.field] = found.type.refersTo + "";
                                        $scope.getUi10Readable(found.type.refersTo, fld.val, key);
                                        $scope.getUi10List(found.type.refersTo);
                                    }
                                    if (found.type.name == 'owner') {
                                        if (value.substring(0, 2) == "19") {
                                            coreBOSWSAPI.doQuery("SELECT last_name, first_name FROM Users WHERE id=" + fld.val).then(function(responseUsr) {
                                                $scope.thisOwner = responseUsr.data.result[0].last_name + " " + responseUsr.data.result[0].first_name
                                            });
                                        } else {
                                            coreBOSWSAPI.doQuery("SELECT groupname FROM Groups WHERE id=" + fld.val).then(function(responseUsr) {
                                                $scope.thisOwner = responseUsr.data.result[0].groupname
                                            });
                                        }
                                    }
                                    if (found.type.name == 'boolean') {
                                        moduleData[key] = ((fld.val == '0' || fld.val == null || fld.val == '') ? false : true);
                                    }
                                    if (found.type.name == 'date') {
                                        $scope.formatedDates[key] = new Date(fld.val);
                                    }
                                    if (found.type.name == 'multipicklist') {
                                        var tempArr = $scope.getModuleValue(fld.field, moduleData, $scope.modulefields);
                                        tempArr = tempArr.split(',');
                                        var tempArrClean = [];
                                        for (var i = 0; i < tempArr.length; i++) {
                                            tempArr[i] = tempArr[i].replace(/^\s*|\s*$/g, '');
                                            if (tempArr[i]) {
                                                tempArrClean.push(tempArr[i])
                                            }
                                        }
                                        chipSelectedOptions[key] = tempArrClean;
                                        tempArr = found.type.picklistValues;
                                        var tempVals = [];
                                        for (var i in tempArr) {
                                            tempVals.push(tempArr[i].value);
                                        }
                                        chipOptions[key] = tempVals;
                                    }

                                    if (found.type.name == 'picklist') {
                                        var pl = {
                                            defaultValue: found.type.defaultValue,
                                            picklistValues: found.type.picklistValues
                                        };
                                        angular.extend(fld, pl);
                                        if (fld.field == 'ticketstatus') {
                                            $scope.ticketstatus = fld;
                                        }
                                    }
                                    cols.push(fld);
                                    if (cols.length == numcols) {
                                        flds.push(cols);
                                        cols = [];
                                    }
                                }
                            });
                            flds.push(cols);
                            $scope.modulefieldList = flds;
                            $scope.moduleData = moduleData;
                            $scope.type10referals = refData;
                            $scope.chipOptions = chipOptions;
                            $scope.chipSelectedOptions = chipSelectedOptions;
                            // console.log("$scope.moduleData44", $scope.moduleData);
                        });
                    }
                });
        }
    });
    $scope.relmodule = $scope.module;
    $scope.relRecordList = [];

    coreBOSWSAPI.getRelatedModules($scope.module, 'DETAILVIEWWIDGET_PORTAL').then(function(response) {
        $scope.relatedmodules = response.data.result;
    });
    $scope.showRel = function(relmod, relfield) {
        if (relmod != $scope.module) {
            coreBOSWSAPI.doDescribe(relmod).then(function(response) {
                $scope.relmodulefields = response.data.result.fields;
                $scope.autocompleteUiRefers($scope.relmodulefields);
                coreBOSWSAPI.doGetUi10RelatedRecords($scope.recordid, $scope.module, relmod, 'DETAILVIEWWIDGET_PORTAL').then(function(response) {
                    $scope.relRecordList = response.data.result.records;
                    $scope.fieldlabels = response.data.result.headers;
                    $scope.fields = response.data.result.fields;
                });
            });
        }
        $scope.relmodule = relmod;
        $scope.relfield = relfield;
    };
    $scope.findattachment = function(id) {
        //alert("called");
        $http.get('http://193.182.16.151/SAME/findattachment.php?id=' + id.split("x")[1]).error(function(data) {
            $scope.tedhenat = data;
            // alert($scope.tedhenat);
            $window.open('http://193.182.16.151/SAME/' + data, "_blank");
        });
        //  $timeout( function(){ alert($scope.tedhenat); }, 3000);
    };
    $scope.doCreateRel = function() {
        alert($scope.entity);
        $scope.entity[$scope.relfield] = $scope.recordid;
        coreBOSWSAPI.doCreate($scope.relmodule, $scope.entity).then(function(response) {
            coreBOSWSAPI.doGetUi10RelatedRecords($scope.recordid, $scope.module, $scope.relmodule, $scope.fields).then(function(response) {
                $scope.relRecordList = response.data.result.records;
            });
        });
    };
    $scope.saveSignature = function saveSignature() {
        var sigPadAPI = $('<div class="sig current" style="display: none;"><canvas class="pad" ng-mouseup="updateModel()" height="240" width="350" name="canvas_sign"></canvas></div>').signaturePad({
            displayOnly: true
        });
        // regenerate the signature onto the canvas
        sigPadAPI.regenerate($scope.helpdesk.signature);
        // convert the canvas to a PNG (Newer versions of Chrome, FF, and Safari only.)
        var firma = sigPadAPI.getSignatureImage().substring(22);
        var filename = $scope.helpdesk.ticket_no + '_Signature.png';
        var model_filename = {
            name: filename,
            size: firma.length,
            type: 'image/png',
            content: firma
        };
        var docData = {
            notes_title: 'Sign: ' + $scope.helpdesk.ticket_title,
            filename: model_filename,
            filetype: model_filename['type'],
            filesize: model_filename['size'],
            filelocationtype: 'I',
            filedownloadcount: 0,
            filestatus: 1,
            folderid: '22x1',
            relations: $scope.helpdesk.id,
        };
        coreBOSWSAPI.doCreate('Documents', docData).then(function(response) {});
    };
    $scope.clearData = function() {
        $scope.helpdesk = {
            signature: null,
        };
    };

    $scope.saveTicketComment = function() {
        coreBOSWSAPI.doAddTicketFaqComment($scope.helpdesk.id, $scope.helpdesk).then(function(response) {
            $scope.helpdesk.comments = null;
        });
    };
})

.controller('relationsCtrl', function($scope, $i18next, $routeParams, coreBOSWSAPI, coreBOSAPIStatus) {
    $scope.recordid = $routeParams.id;
    $scope.module = $routeParams.srcmodule;
    $scope.relmodule = $routeParams.rlmodule;
    $scope.relRecordList = [];
    $scope.showModal = false;
    $scope.toggleModal = function() {
        $scope.showModal = !$scope.showModal;
    };
    $scope.addEntity = function() {
        //   alert('inhere');
        var messagesname = document.getElementById('messagesname').value;
        var description = document.getElementById('description').value;
        // alert($scope.recordid);
        // alert(messagesname);
        // alert(description);
        var casemessage = $scope.recordid.split("x");
        var casemessage1 = casemessage[1];
        var smsData = {
            messagesname: messagesname,
            description: description,
            casemessage: casemessage1
        };
        coreBOSWSAPI.doCreate('Messages', smsData).then(function($scope, response) {

            alert("created");


        });


    };
    coreBOSWSAPI.doGetRelatedRecords($scope.recordid, $scope.module, $scope.relmodule, "").then(function(response) {
        $scope.relRecordList = response.data.result.records;
    });
    $scope.doCreate = function() {
        $scope.entity['related_to'] = $scope.recordid;
        $scope.entity['sales_stage'] = 'Negotiation/Review';
        $scope.srelresult = [];
        coreBOSWSAPI.doCreate($scope.module, $scope.entity).then(function(response) {
            coreBOSWSAPI.doGetRelatedRecords($scope.recordid, $scope.module, $scope.relmodule, "").then(function(response) {
                $scope.relRecordList = response.data.result.records;
            });
        });
    };
    $scope.sendgrelCall = function() {
        $scope.grelresult = "";
        var queryParameters = {};
        if (!angular.isUndefined($scope.rpdodisc)) queryParameters.productDiscriminator = $scope.rpdodisc;
        if (!angular.isUndefined($scope.glimit)) queryParameters.limit = $scope.glimit;
        if (!angular.isUndefined($scope.groffset)) queryParameters.offset = $scope.groffset;
        if (!angular.isUndefined($scope.gorder)) queryParameters.orderby = $scope.gorder;
        if (!angular.isUndefined($scope.gcols)) queryParameters.columns = $scope.gcols;
        coreBOSWSAPI.doGetRelatedRecords($scope.grelrecord, $scope.gparentmodule, $scope.grelmodule, queryParameters).then(function(response) {
            $scope.grelresult = response.data;
        });
    };
    $scope.sendsrelCall = function() {
        $scope.srelresult = "";
        coreBOSWSAPI.doSetRelated($scope.srelrecord, $scope.srelwith).then(function(response) {
            $scope.srelresult = response.data;
        });
    };
})
.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }                                       
            });
        }
    };
});
