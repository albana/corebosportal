'use strict';

angular.module('coreBOSJSTickets', ['ngRoute', 'coreBOSJSTickets.setup', 'ngSanitize', 'coreBOSJSTickets.filters', 'coreBOSAPIservice',
  'coreBOSJSTickets.directives', 'coreBOSJSTickets.controllers', 'angular-md5', 'ui.select', 'am.date-picker',
  'jm.i18next', 'ngTable', 'nvd3', 'ui.calendar', 'ngMaterial', 'ngAnimate', 'mgcrea.ngStrap'
])


.config(function($popoverProvider) {
  angular.extend($popoverProvider.defaults, {
    html: true
  });
})
.config(function($mdDateLocaleProvider) {
    $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('YYYY-MM-DD');
    };
})
.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('orange')
      .accentPalette('orange');
  })
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/config', {
      templateUrl: 'partials/config.html',
      controller: 'configCtrl'
    });
    $routeProvider.when('/logout', {
      templateUrl: 'partials/login.html',
      controller: 'logoutCtrl'
    });
    $routeProvider.when('/login', {
      templateUrl: 'partials/login.html',
      controller: 'loginCtrl'
    });
    $routeProvider.when('/portal_module/:module/:moduleLabel', {
      templateUrl: 'partials/portal_module.html',
      controller: 'moduleCtrl'
    });
    $routeProvider.when('/portal_moduleview/:module/:id', {
      templateUrl: 'partials/portal_moduleview.html',
      controller: 'moduleViewCtrl'
    });
    $routeProvider.when('/portal_createview/:module', {
      templateUrl: 'partials/portal_createview.html',
      controller: 'CreateViewCtrl'
    });
    $routeProvider.when('/portal_createview/:module/:relmodule/:id/:relatedfield', {
      templateUrl: 'partials/portal_createview.html',
      controller: 'CreateViewCtrlRelation'
    });
    $routeProvider.when('/portal_relatedList/:srcmodule/:relmodule/:id', {
      templateUrl: 'partials/portal_relatedList.html',
      controller: 'relationsCtrl'
    });
    $routeProvider.when('/portal_calendar', {
      templateUrl: 'partials/portal_calendar.html',
      controller: 'CustomCalendarCtrl'
    });
    $routeProvider.when('/portal_home', {
      templateUrl: 'partials/portal_home.html',
      controller: 'HomeCtrl'
    });
    $routeProvider.when('/portal_dashboards/:name/:nameLabel', {
      templateUrl: 'partials/portal_stockmonitor.html',
      controller: 'StockMonitor'
    });
    $routeProvider.otherwise({
      redirectTo: '/portal_module/Accounts/Customers'
    });
  }])
  .config(['Setup', '$i18nextProvider', function(Setup, $i18nextProvider) {
    $i18nextProvider.options = {
      lng: Setup.language,
      useCookie: true,
      useLocalStorage: false,
      fallbackLng: 'en',
      resGetPath: 'locales/__lng__/translation.json',
      defaultLoadingValue: '' // ng-i18next option, *NOT* directly supported by i18next
    };
  }])
  .config(['amDatePickerConfigProvider', function(amDatePickerConfigProvider) {
        amDatePickerConfigProvider.setOptions({
            calendarIcon: 'https://raw.githubusercontent.com/unmade/am-date-picker/master/dist/images/icons/ic_today_24px.svg',
            clearIcon: 'https://raw.githubusercontent.com/unmade/am-date-picker/master/dist/images/icons/ic_close_24px.svg',
            nextIcon: 'https://raw.githubusercontent.com/unmade/am-date-picker/master/dist/images/icons/ic_chevron_right_18px.svg',
            prevIcon: 'https://raw.githubusercontent.com/unmade/am-date-picker/master/dist/images/icons/ic_chevron_left_18px.svg'
        })
    }])
  .run(function(Setup, $rootScope, coreBOSAPIStatus, coreBOSWSAPI, $location, $window) {
    $rootScope.$on('$routeChangeStart', function(ev, next, curr) {
        
      var expTime = 1800;
      var now = moment().unix();

      coreBOSAPIStatus.setServerTime(now);

      if (coreBOSAPIStatus.getExpireTime() < coreBOSAPIStatus.getServerTime()) {
        coreBOSAPIStatus.setInvalidKeys(true);
        coreBOSAPIStatus.setSessionInfo({});
        $location.path('/login');
        $rootScope.location = $location;
      }

      coreBOSAPIStatus.setExpireTime(now + expTime);
      
      $rootScope.menuShow = true;
      if (next.$$route) {
        if (coreBOSAPIStatus.hasInvalidKeys()) {
          $location.path('/login');
          $rootScope.location = $location;
        } else {
          if ($window.innerWidth < 768) {
            $rootScope.menuShow = false;
          }
        }
      }
    });
    var trgesTranslation = {
      "Search": "Buscar",
      "Page": "Página",
      "First Page": "Primera",
      "Next Page": "Siguiente",
      "Previous Page": "Anterior",
      "Last Page": "Ultima",
      "Sort": "Ordenar",
      "No items to display": "No hay registros",
      "displayed": "mostrados",
      "in total": "en total"
    };
    //TrNgGrid.translations['es'] = trgesTranslation;
  });
